﻿using System.Windows.Forms;
using Player.Custom;
using Player.Network;

namespace Player
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            ShowLoginPage();
            NetworkMain.ConnectServer();
            FuncGlobal.ChangeTitleFullName = SetTitleFullName;
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }

        private void SetTitleFullName()
        {
            if (DefaultValue.MyUser != null)
            {
                TitleFullName.Text = DefaultValue.MyUser.FullName;
            }
        }

    }
}
