﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Player.Custom;
using Player.Network;

namespace Player.Layout
{
    public partial class LoginExam : UserControl
    {
        private readonly Action _loginSuccessAction;
        public LoginExam(Action loginSuccessAction)
        {
            InitializeComponent();
            _loginSuccessAction = loginSuccessAction;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BeginInvoke((Action) (() =>
            {
                JoinExam.Enabled = false;
            }));
            if (txtExamName.Text.Equals(string.Empty) || txtExamPassWord.Text.Equals(String.Empty))
            {
                Notify.Error("Bạn cần nhập đủ mã cuộc thi và mật khẩu của nó");
            }
            LoginExamReq loginExamReq = new LoginExamReq
            {
                ExamId = int.Parse(txtExamName.Text),
                PassWord = txtExamPassWord.Text,
                RequestName = (int) RequestName.LoginExam,
                UserName = DefaultValue.MyUser.UserName
            };
            NetworkMain.Send(loginExamReq, OnResponseLoginExam);
        }

        private void OnResponseLoginExam(string data)
        {
            LoginExamRes loginExamRes = NetworkUtil.ParseJson<LoginExamRes>(data);
            if (loginExamRes.ResourceKey == (int)ResourceKey.Success)
            {
                DefaultValue.ExamId = int.Parse(txtExamName.Text);
                _loginSuccessAction();
            }
            else
            {
                Notify.Warning("Thông tin cuộc thi không chính xác");
                BeginInvoke((Action)(() =>
                {
                    JoinExam.Enabled = false;
                }));
            }
        }

        private void Login_Txt_UserName_Enter(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals("Mã cuộc thi"))
            {
                textBoxHere.Text = string.Empty;
                textBoxHere.ForeColor = Color.Black;
            }
        }

        private void Login_Txt_UserName_Leave(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals(String.Empty))
            {
                textBoxHere.Text = "Mã cuộc thi";
                textBoxHere.ForeColor = SystemColors.ButtonShadow;
            }
        }

        private void Login_Txt_PassWord_Enter(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals("Mật khẩu"))
            {
                textBoxHere.Text = string.Empty;
                textBoxHere.ForeColor = Color.Black;
                textBoxHere.PasswordChar = '*';
            }
        }

        private void Login_Txt_PassWord_Leave(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals(String.Empty))
            {
                textBoxHere.Text = "Mật khẩu";
                textBoxHere.ForeColor = SystemColors.ButtonShadow;
                textBoxHere.PasswordChar = '\0';
            }
        }

    }
}
