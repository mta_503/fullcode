﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Newtonsoft.Json;
using Player.Custom;
using Player.Entity;
using Player.Network;

namespace Player.Layout
{

    public partial class PlayExam : UserControl
    {
        private Question _currentQuestion;
        private Dictionary<string, UserControlName> _dicUserControlName;


        public PlayExam()
        {
            _dicUserControlName = new Dictionary<string, UserControlName>();
            InitializeComponent();
            BroadcastFunction.OnResQuestion = OnresQuestion;
            BroadcastFunction.OnResUserInfo = OnResUserInfo;
            BroadcastFunction.OnResTime = OnResTime;
            BroadcastFunction.OnResAnswer = OnResAnswer;
            BroadcastFunction.OnResCalculPointKd = OnResCalculPointKd;
        }

        private void LayoutPlayer()
        {
            BeginInvoke((Action)(() =>
           {
               ButtonSubmit.Visible = true;
               AnswerInput.Visible = true;
           }));
        }

        private void LayoutViewer()
        {
            BeginInvoke((Action)(() =>
            {
                ButtonSubmit.Visible = false;
                AnswerInput.Visible = false;
            }));
        }

        private void ShowAllControl(int index, UserInfoExam examQuestion)
        {
            try
            {
                BeginInvoke((Action)(() =>
                {
                    Label pointLabel = PointPanel.Controls.Find("PointLabel" + index, true).FirstOrDefault() as Label;
                    if (pointLabel != null)
                    {
                        pointLabel.Text = examQuestion.Point.ToString();
                        _dicUserControlName[examQuestion.User.UserName].Point = "PointLabel" + index;
                    }
                    Label userNameLabel = PointPanel.Controls.Find("UserNameLabel" + index, true).FirstOrDefault() as Label;
                    if (userNameLabel != null)
                    {
                        userNameLabel.Text = examQuestion.User.FullName;
                        _dicUserControlName[examQuestion.User.UserName].FullName = "UserNameLabel" + index;
                    }
                    Panel mainPanel = PointPanel.Controls.Find("PointPanelChild" + index, true).FirstOrDefault() as Panel;
                    if (mainPanel != null)
                    {
                        mainPanel.Visible = true;
                    }
                    Button buttonRight = PointPanel.Controls.Find("Right" + index, true).FirstOrDefault() as Button;
                    if (buttonRight != null && DefaultValue.MyUser.Role == (int)Role.Admin)
                    {
                        buttonRight.Visible = true;
                        _dicUserControlName[examQuestion.User.UserName].RightButton = "Right" + index;
                    }
                    Button buttonFail = PointPanel.Controls.Find("Fail" + index, true).FirstOrDefault() as Button;
                    if (buttonFail != null && DefaultValue.MyUser.Role == (int)Role.Admin)
                    {
                        buttonFail.Visible = true;
                        _dicUserControlName[examQuestion.User.UserName].FailButton = "Fail" + index;
                    }
                    TextBox answerOutput = PointPanel.Controls.Find("AnswerOutput" + index, true).FirstOrDefault() as TextBox;
                    if (answerOutput != null)
                    {
                        answerOutput.Visible = true;
                        _dicUserControlName[examQuestion.User.UserName].AnswerView = "AnswerOutput" + index;
                    }
                }));
            }
            catch (Exception e)
            {
                Notify.Error(e.ToString());
            }
        }


        public void OnResUserInfo(string data)
        {

            BroadcastUserInfoExam dataFormat = NetworkUtil.ParseJson<BroadcastUserInfoExam>(data);
            for (int i = 0; i < dataFormat.ListUserInfoExam.Count; i++)
            {
                //code can toi uu
                if (dataFormat.ListUserInfoExam[i].User.Role != (int)Role.Admin)
                {
                    _dicUserControlName[dataFormat.ListUserInfoExam[i].User.UserName] = new UserControlName();
                    ShowAllControl(i + 1, dataFormat.ListUserInfoExam[i]);
                }
            }
            BeginInvoke((Action)(() =>
            {
                if (DefaultValue.MyUser.Role == (int)Role.Admin)
                {
                    ViewAnswer.Visible = true;
                    btnNext.Visible = true;
                }
            }));

        }

        public void OnresQuestion(string data)
        {

            BeginInvoke((Action)(() =>
           {
               BroadcastExamQuestion dataFormat = NetworkUtil.ParseJson<BroadcastExamQuestion>(data);
               _currentQuestion =  dataFormat.Question;
             
               QuestionLabel.Text = "Câu số "+  (dataFormat.QuestionIndex + 1).ToString() + "/12  " + dataFormat.Question.QuesCont;
               
               if (DefaultValue.MyUser.UserName.Equals(dataFormat.UserName))
                   LayoutPlayer();
               else LayoutViewer();

               if (dataFormat.Question.QuesType == (int)QuestionType.Choice)
               {

                       
                   for (int i = 0; i < dataFormat.ListAnswer.Count; i++)
                   {
                       Label answer =
                           AnswerPanel.Controls.Find("answerChoice" + (i + 1), true).FirstOrDefault() as Label;
                       // Check times up 
                       if (answer != null /*&& Int32.Parse(TimeExamLabel.Text.ToString()) >= 0*/)
                       {
                           answer.Visible = true;
                           answer.Text = dataFormat.ListAnswer[i].AnsName + ": " + dataFormat.ListAnswer[i].AnsCont;
                       }
                   }

               }
               else
               {
                   /*  if quesion type is not choise clear answer previous  question */
                   for (int j = 0; j < 4; j++)
                   {
                       Label noAnswer =
                            AnswerPanel.Controls.Find("answerChoice" + (j + 1), true).FirstOrDefault() as Label;
                       // Check times up 
                       if (noAnswer != null /*&& Int32.Parse(TimeExamLabel.Text.ToString()) >= 0*/)
                       {
                           noAnswer.Text = "";
                          
                       }
                   }
               }
           }));
        }

        public void OnResTime(string data)
        {
            BeginInvoke((Action)(() =>
           {
               BroadcastTimeExam dataFormat = NetworkUtil.ParseJson<BroadcastTimeExam>(data);
               TimeExamLabel.Text = dataFormat.TimeCurrentValue.ToString();
           }));
        }

        private void ViewAnswer_Click(object sender, EventArgs e)
        {
            ViewAnswer viewAnswerForm = new ViewAnswer(_currentQuestion.Answer, _currentQuestion.Explain);
            viewAnswerForm.Show();
        }

        private void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (AnswerInput.Text.Equals(String.Empty))
            {
                Notify.Error("Bạn cần nhập đáp án");
                return;
            }
            AnswerKdReq answerKdReq = new AnswerKdReq
            {
                UserName = DefaultValue.MyUser.UserName,
                ExamId = DefaultValue.ExamId,
                UserAnswer = AnswerInput.Text,
                RequestName = (int)RequestName.AnswerKd
            };

            NetworkMain.Send(answerKdReq, null);
            AnswerInput.Text = "";
    
        }

        private void OnResAnswer(string data)
        {
            BroadcastAnswerKd broadcastAnswerKd = NetworkUtil.ParseJson<BroadcastAnswerKd>(data);
            if (broadcastAnswerKd != null)
            {
                if (_dicUserControlName.ContainsKey(broadcastAnswerKd.UserName))
                {
                    var answerView = _dicUserControlName[broadcastAnswerKd.UserName].AnswerView;
                    if (answerView != null)
                    {
                        TextBox answer =
                           PointPanel.Controls.Find(answerView, true).FirstOrDefault() as TextBox;
                        if (answer != null)
                        {
                            answer.Text = broadcastAnswerKd.Answer;
                        }
                    }
                }
            }
        }

        private void ButtonSubmit_MouseHover(object sender, EventArgs e)
        {

        }

        private void OnResCalculPointKd(string data)
        {

            BeginInvoke((Action)(() =>
            {
                BroadcastPointUserKd dataFormat = NetworkUtil.ParseJson<BroadcastPointUserKd>(data);
                if (dataFormat != null)
                {
                    if (_dicUserControlName.ContainsKey(dataFormat.UserName))
                    {
                        Label pointLabel =
                               PointPanel.Controls.Find(_dicUserControlName[dataFormat.UserName].Point, true).FirstOrDefault() as Label;
                        if (pointLabel != null)
                        {
                            pointLabel.Text = dataFormat.Point.ToString();
                        }
                    }
                }
            }));
        }

        private void Right_Click(object sender, EventArgs e)
        {
            Button rightButton = (Button)sender;
            if (rightButton != null)
            {
                string userName =
                    _dicUserControlName.Where(p => p.Value.RightButton.Equals(rightButton.Name)).Select(p => p.Key).FirstOrDefault();
                if (userName != null && _currentQuestion.Point != null)
                {
                    CalculatePointKdReq request = new CalculatePointKdReq
                    {
                        UserName = userName,
                        ExamId = DefaultValue.ExamId,
                        Point = (int)_currentQuestion.Point,
                        RequestName = (int)RequestName.CalculatePointKd
                    };
                    NetworkMain.Send(request, null);
                }
            }
        }

        private void Left_Click(object sender, EventArgs e)
        {
            Button rightButton = (Button)sender;
            if (rightButton != null)
            {
                string userName =
                    _dicUserControlName.Where(p => p.Value.FailButton.Equals(rightButton.Name)).Select(p => p.Key).FirstOrDefault();
                if (userName != null && _currentQuestion.Point != null)
                {
                    CalculatePointKdReq request = new CalculatePointKdReq
                    {
                        UserName = userName,
                        ExamId = DefaultValue.ExamId,
                        Point = (int)_currentQuestion.Point * -1,
                        RequestName = (int)RequestName.CalculatePointKd
                    };
                    NetworkMain.Send(request, null);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            
            NextKdReq nextKdReq = new NextKdReq
            {
                RequestName = (int)RequestName.NextKd,
                ExamId = DefaultValue.ExamId
            };
            if (Int32.Parse(TimeExamLabel.Text.ToString()) == 0)
            {
                nextKdReq.IsEndSession = true;
                MessageBox.Show("Chuyển sang người chơi tiếp theo ","", MessageBoxButtons.OK);
                NetworkMain.Send(nextKdReq, null);

            }else
            {
                NetworkMain.Send(nextKdReq, null);
            }

           
        }

        private void TimeExamLabel_Click(object sender, EventArgs e)
        {

        }

        private void QuestionLabel_Click(object sender, EventArgs e)
        {

        }

        private void TitleQuestion_Click(object sender, EventArgs e)
        {

        }

        private void QuestionLabel_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
