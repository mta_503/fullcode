﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Player.Custom;
using Player.Entity;
using Player.Network;

namespace Player.Layout
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            Init_QuestionKd();
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void Ques_KD_Add_BtnAdd_Click(object sender, EventArgs e)
        {
            UpdateQuestionReq updateQuestionReq = new UpdateQuestionReq();
            Question newQuestion = new Question();
            newQuestion.QuesCont = Ques_KD_Add_TxtQuesCont.Text;
            newQuestion.Answer = Ques_KD_Add_Answer.Text;
            var grade = Operator.GetGradeIdByName(Ques_KD_Add_Grade.Text);
            if (grade > 0)
            {
                newQuestion.Grade = grade;
            }
            else
            {
                Notify.Error("Grade Not Exist");
                return;
            }
            var level = Operator.GetLevelIdByName(Ques_KD_Add_Level.Text);
            if (level > 0)
            {
                newQuestion.Level = level;
            }
            else
            {
                Notify.Error("Level Not Exist");
                return;
            }
            var quesType = Operator.GetQuestTypeIdByName(Ques_KD_Add_QuesType.Text);
            if (quesType > 0)
            {
                newQuestion.QuesType = quesType;
            }
            else
            {
                Notify.Error("QuestionType Not Exist");
                return;
            }
            newQuestion.Explain = Ques_KD_Add_Explain.Text;
            newQuestion.Point = long.Parse(Ques_KD_Add_Point.Text);
            newQuestion.Cat = Ques_KD_Add_Topic.Text;
            newQuestion.QuesStatus = (long)QuestionStatus.Active;
            newQuestion.ContestType = (long) ContestType.Kd;

            updateQuestionReq.Question = newQuestion;
            updateQuestionReq.RequestName = (int) RequestName.UpdateQuestion;
            updateQuestionReq.RequestType = (int) RequestType.Insert;

            if (newQuestion.QuesType == (int)QuestionType.Choice)
            {
                List<Answer> answers = new List<Answer>();

                foreach (DataGridViewRow row in Ques_KD_Add_ListAnswer.Rows)
                {
                    if (row.Cells["AnsName"].Value == null)
                    {
                        continue;
                    }
                    answers.Add(new Answer
                    {
                        AnsName = row.Cells["AnsName"].Value.ToString(),
                        AnsCont = row.Cells["AnsCont"].Value.ToString()
                    });
                }

                updateQuestionReq.Answers = answers;
            }

            NetworkMain.Send(updateQuestionReq,OnresUpdateQuestion);

        }

        private void OnresUpdateQuestion(string data)
        {
            UpdateQuestionRes updateQuestionRes = NetworkUtil.ParseJson<UpdateQuestionRes>(data);
            Notify.Error("ngon ve response roi");
        }

        private void tabPage10_Click(object sender, EventArgs e)
        {
            
        }

        private void tabControl4_Selected(object sender, TabControlEventArgs e)
        {
            foreach (var topic in DefaultValue.TopicList)
            {
                Ques_KD_Add_Topic.Items.Add(topic);
            }
            Ques_KD_Add_Topic.SelectedIndex = 0;
            Ques_KD_Add_Topic.Sorted = true;
        }

        private void tabControl3_Selected(object sender, TabControlEventArgs e)
        {
            
        }

        private void OnResponseGetListQuestion(string data)
        {
            GetListQuestionRes getListQuestionRes = NetworkUtil.ParseJson<GetListQuestionRes>(data);

            List<Question> listQuestion = getListQuestionRes.ListQuestion;
            for (int i = 0; i < listQuestion.Count; i++)
            {
                var question = listQuestion[i];
                Dgv_ListQuestion_KD.Rows[i].Cells[0].Value = question.QuesId;
                Dgv_ListQuestion_KD.Rows[i].Cells[1].Value = question.QuesCont;
                Dgv_ListQuestion_KD.Rows[i].Cells[2].Value = Operator.GetGradeName(question.Grade);
                Dgv_ListQuestion_KD.Rows[i].Cells[3].Value = Operator.GetLevelName(question.Level);
                Dgv_ListQuestion_KD.Rows[i].Cells[4].Value = question.Cat;
                Dgv_ListQuestion_KD.Rows[i].Cells[5].Value = question.Answer;
            }
        }

        private void tabControl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        public void Init_QuestionKd()
        {
            GetListQuestionReq getListQuestionReq = new GetListQuestionReq();
            getListQuestionReq.RequestName = (int)RequestName.GetListQuestion;
            getListQuestionReq.ContestType = (int)ContestType.Kd;
            getListQuestionReq.RequestType = (int)RequestType.GetByContestType;

            NetworkMain.Send(getListQuestionReq, OnResponseGetListQuestion);
        }

    }
}
