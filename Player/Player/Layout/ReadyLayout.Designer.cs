﻿namespace Player.Layout
{
    partial class ReadyLayout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.User1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Status1 = new System.Windows.Forms.Label();
            this.FullName1 = new System.Windows.Forms.Label();
            this.UserName1 = new System.Windows.Forms.Label();
            this.User2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Status2 = new System.Windows.Forms.Label();
            this.FullName2 = new System.Windows.Forms.Label();
            this.UserName2 = new System.Windows.Forms.Label();
            this.User3 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Status3 = new System.Windows.Forms.Label();
            this.FullName3 = new System.Windows.Forms.Label();
            this.UserName3 = new System.Windows.Forms.Label();
            this.User4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.Status4 = new System.Windows.Forms.Label();
            this.FullName4 = new System.Windows.Forms.Label();
            this.UserName4 = new System.Windows.Forms.Label();
            this.User5 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Status5 = new System.Windows.Forms.Label();
            this.FullName5 = new System.Windows.Forms.Label();
            this.UserName5 = new System.Windows.Forms.Label();
            this.User6 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Status6 = new System.Windows.Forms.Label();
            this.FullName6 = new System.Windows.Forms.Label();
            this.UserName6 = new System.Windows.Forms.Label();
            this.bntReady = new System.Windows.Forms.Button();
            this.AllUserReady = new System.Windows.Forms.Panel();
            this.User1.SuspendLayout();
            this.User2.SuspendLayout();
            this.User3.SuspendLayout();
            this.User4.SuspendLayout();
            this.User5.SuspendLayout();
            this.User6.SuspendLayout();
            this.AllUserReady.SuspendLayout();
            this.SuspendLayout();
            // 
            // User1
            // 
            this.User1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.User1.Controls.Add(this.panel2);
            this.User1.Controls.Add(this.Status1);
            this.User1.Controls.Add(this.FullName1);
            this.User1.Controls.Add(this.UserName1);
            this.User1.Location = new System.Drawing.Point(84, 37);
            this.User1.Name = "User1";
            this.User1.Size = new System.Drawing.Size(251, 139);
            this.User1.TabIndex = 0;
            this.User1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::Player.Properties.Resources.boy;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(71, 70);
            this.panel2.TabIndex = 1;
            // 
            // Status1
            // 
            this.Status1.AutoSize = true;
            this.Status1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status1.ForeColor = System.Drawing.Color.LimeGreen;
            this.Status1.Location = new System.Drawing.Point(79, 95);
            this.Status1.Name = "Status1";
            this.Status1.Size = new System.Drawing.Size(97, 24);
            this.Status1.TabIndex = 0;
            this.Status1.Text = "Sẵn sàng";
            // 
            // FullName1
            // 
            this.FullName1.AutoSize = true;
            this.FullName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName1.Location = new System.Drawing.Point(76, 45);
            this.FullName1.Name = "FullName1";
            this.FullName1.Size = new System.Drawing.Size(142, 16);
            this.FullName1.TabIndex = 0;
            this.FullName1.Text = "Nguyễn Huy Hoàng";
            // 
            // UserName1
            // 
            this.UserName1.AutoSize = true;
            this.UserName1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName1.Location = new System.Drawing.Point(80, 13);
            this.UserName1.Name = "UserName1";
            this.UserName1.Size = new System.Drawing.Size(111, 16);
            this.UserName1.TabIndex = 0;
            this.UserName1.Text = "nguyenhuyhoang";
            // 
            // User2
            // 
            this.User2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.User2.Controls.Add(this.panel1);
            this.User2.Controls.Add(this.Status2);
            this.User2.Controls.Add(this.FullName2);
            this.User2.Controls.Add(this.UserName2);
            this.User2.Location = new System.Drawing.Point(357, 37);
            this.User2.Name = "User2";
            this.User2.Size = new System.Drawing.Size(251, 139);
            this.User2.TabIndex = 1;
            this.User2.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::Player.Properties.Resources.boy;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(71, 70);
            this.panel1.TabIndex = 1;
            // 
            // Status2
            // 
            this.Status2.AutoSize = true;
            this.Status2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status2.ForeColor = System.Drawing.Color.LimeGreen;
            this.Status2.Location = new System.Drawing.Point(79, 95);
            this.Status2.Name = "Status2";
            this.Status2.Size = new System.Drawing.Size(97, 24);
            this.Status2.TabIndex = 0;
            this.Status2.Text = "Sẵn sàng";
            // 
            // FullName2
            // 
            this.FullName2.AutoSize = true;
            this.FullName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName2.Location = new System.Drawing.Point(76, 45);
            this.FullName2.Name = "FullName2";
            this.FullName2.Size = new System.Drawing.Size(142, 16);
            this.FullName2.TabIndex = 0;
            this.FullName2.Text = "Nguyễn Huy Hoàng";
            // 
            // UserName2
            // 
            this.UserName2.AutoSize = true;
            this.UserName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName2.Location = new System.Drawing.Point(80, 13);
            this.UserName2.Name = "UserName2";
            this.UserName2.Size = new System.Drawing.Size(111, 16);
            this.UserName2.TabIndex = 0;
            this.UserName2.Text = "nguyenhuyhoang";
            // 
            // User3
            // 
            this.User3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.User3.Controls.Add(this.panel3);
            this.User3.Controls.Add(this.Status3);
            this.User3.Controls.Add(this.FullName3);
            this.User3.Controls.Add(this.UserName3);
            this.User3.Location = new System.Drawing.Point(626, 37);
            this.User3.Name = "User3";
            this.User3.Size = new System.Drawing.Size(251, 139);
            this.User3.TabIndex = 2;
            this.User3.Visible = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = global::Player.Properties.Resources.boy;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(71, 70);
            this.panel3.TabIndex = 1;
            // 
            // Status3
            // 
            this.Status3.AutoSize = true;
            this.Status3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status3.ForeColor = System.Drawing.Color.LimeGreen;
            this.Status3.Location = new System.Drawing.Point(79, 95);
            this.Status3.Name = "Status3";
            this.Status3.Size = new System.Drawing.Size(97, 24);
            this.Status3.TabIndex = 0;
            this.Status3.Text = "Sẵn sàng";
            // 
            // FullName3
            // 
            this.FullName3.AutoSize = true;
            this.FullName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName3.Location = new System.Drawing.Point(76, 45);
            this.FullName3.Name = "FullName3";
            this.FullName3.Size = new System.Drawing.Size(142, 16);
            this.FullName3.TabIndex = 0;
            this.FullName3.Text = "Nguyễn Huy Hoàng";
            // 
            // UserName3
            // 
            this.UserName3.AutoSize = true;
            this.UserName3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName3.Location = new System.Drawing.Point(80, 13);
            this.UserName3.Name = "UserName3";
            this.UserName3.Size = new System.Drawing.Size(111, 16);
            this.UserName3.TabIndex = 0;
            this.UserName3.Text = "nguyenhuyhoang";
            // 
            // User4
            // 
            this.User4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.User4.Controls.Add(this.panel6);
            this.User4.Controls.Add(this.Status4);
            this.User4.Controls.Add(this.FullName4);
            this.User4.Controls.Add(this.UserName4);
            this.User4.Location = new System.Drawing.Point(84, 195);
            this.User4.Name = "User4";
            this.User4.Size = new System.Drawing.Size(251, 139);
            this.User4.TabIndex = 3;
            this.User4.Visible = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.BackgroundImage = global::Player.Properties.Resources.boy;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Location = new System.Drawing.Point(3, 5);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(71, 70);
            this.panel6.TabIndex = 1;
            // 
            // Status4
            // 
            this.Status4.AutoSize = true;
            this.Status4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status4.ForeColor = System.Drawing.Color.LimeGreen;
            this.Status4.Location = new System.Drawing.Point(79, 95);
            this.Status4.Name = "Status4";
            this.Status4.Size = new System.Drawing.Size(97, 24);
            this.Status4.TabIndex = 0;
            this.Status4.Text = "Sẵn sàng";
            // 
            // FullName4
            // 
            this.FullName4.AutoSize = true;
            this.FullName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName4.Location = new System.Drawing.Point(76, 45);
            this.FullName4.Name = "FullName4";
            this.FullName4.Size = new System.Drawing.Size(142, 16);
            this.FullName4.TabIndex = 0;
            this.FullName4.Text = "Nguyễn Huy Hoàng";
            // 
            // UserName4
            // 
            this.UserName4.AutoSize = true;
            this.UserName4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName4.Location = new System.Drawing.Point(80, 13);
            this.UserName4.Name = "UserName4";
            this.UserName4.Size = new System.Drawing.Size(111, 16);
            this.UserName4.TabIndex = 0;
            this.UserName4.Text = "nguyenhuyhoang";
            // 
            // User5
            // 
            this.User5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.User5.Controls.Add(this.panel5);
            this.User5.Controls.Add(this.Status5);
            this.User5.Controls.Add(this.FullName5);
            this.User5.Controls.Add(this.UserName5);
            this.User5.Location = new System.Drawing.Point(357, 195);
            this.User5.Name = "User5";
            this.User5.Size = new System.Drawing.Size(251, 139);
            this.User5.TabIndex = 4;
            this.User5.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.BackgroundImage = global::Player.Properties.Resources.boy;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Location = new System.Drawing.Point(3, 7);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(71, 70);
            this.panel5.TabIndex = 1;
            // 
            // Status5
            // 
            this.Status5.AutoSize = true;
            this.Status5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status5.ForeColor = System.Drawing.Color.LimeGreen;
            this.Status5.Location = new System.Drawing.Point(79, 95);
            this.Status5.Name = "Status5";
            this.Status5.Size = new System.Drawing.Size(97, 24);
            this.Status5.TabIndex = 0;
            this.Status5.Text = "Sẵn sàng";
            // 
            // FullName5
            // 
            this.FullName5.AutoSize = true;
            this.FullName5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName5.Location = new System.Drawing.Point(76, 45);
            this.FullName5.Name = "FullName5";
            this.FullName5.Size = new System.Drawing.Size(142, 16);
            this.FullName5.TabIndex = 0;
            this.FullName5.Text = "Nguyễn Huy Hoàng";
            // 
            // UserName5
            // 
            this.UserName5.AutoSize = true;
            this.UserName5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName5.Location = new System.Drawing.Point(80, 13);
            this.UserName5.Name = "UserName5";
            this.UserName5.Size = new System.Drawing.Size(111, 16);
            this.UserName5.TabIndex = 0;
            this.UserName5.Text = "nguyenhuyhoang";
            // 
            // User6
            // 
            this.User6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.User6.Controls.Add(this.panel4);
            this.User6.Controls.Add(this.Status6);
            this.User6.Controls.Add(this.FullName6);
            this.User6.Controls.Add(this.UserName6);
            this.User6.Location = new System.Drawing.Point(626, 195);
            this.User6.Name = "User6";
            this.User6.Size = new System.Drawing.Size(251, 139);
            this.User6.TabIndex = 5;
            this.User6.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::Player.Properties.Resources.boy;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(3, 5);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(71, 70);
            this.panel4.TabIndex = 1;
            // 
            // Status6
            // 
            this.Status6.AutoSize = true;
            this.Status6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status6.ForeColor = System.Drawing.Color.LimeGreen;
            this.Status6.Location = new System.Drawing.Point(79, 95);
            this.Status6.Name = "Status6";
            this.Status6.Size = new System.Drawing.Size(97, 24);
            this.Status6.TabIndex = 0;
            this.Status6.Text = "Sẵn sàng";
            // 
            // FullName6
            // 
            this.FullName6.AutoSize = true;
            this.FullName6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName6.Location = new System.Drawing.Point(76, 45);
            this.FullName6.Name = "FullName6";
            this.FullName6.Size = new System.Drawing.Size(142, 16);
            this.FullName6.TabIndex = 0;
            this.FullName6.Text = "Nguyễn Huy Hoàng";
            // 
            // UserName6
            // 
            this.UserName6.AutoSize = true;
            this.UserName6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName6.Location = new System.Drawing.Point(80, 13);
            this.UserName6.Name = "UserName6";
            this.UserName6.Size = new System.Drawing.Size(111, 16);
            this.UserName6.TabIndex = 0;
            this.UserName6.Text = "nguyenhuyhoang";
            // 
            // bntReady
            // 
            this.bntReady.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntReady.Location = new System.Drawing.Point(444, 431);
            this.bntReady.Name = "bntReady";
            this.bntReady.Size = new System.Drawing.Size(128, 36);
            this.bntReady.TabIndex = 1;
            this.bntReady.Text = "Sẵn sàng";
            this.bntReady.UseVisualStyleBackColor = true;
            this.bntReady.Click += new System.EventHandler(this.bntReady_Click);
            // 
            // AllUserReady
            // 
            this.AllUserReady.BackgroundImage = global::Player.Properties.Resources.anh_du_thi_so_6_ha_long_ve_dem__2_;
            this.AllUserReady.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.AllUserReady.Controls.Add(this.User6);
            this.AllUserReady.Controls.Add(this.bntReady);
            this.AllUserReady.Controls.Add(this.User5);
            this.AllUserReady.Controls.Add(this.User4);
            this.AllUserReady.Controls.Add(this.User3);
            this.AllUserReady.Controls.Add(this.User2);
            this.AllUserReady.Controls.Add(this.User1);
            this.AllUserReady.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllUserReady.Location = new System.Drawing.Point(0, 0);
            this.AllUserReady.Name = "AllUserReady";
            this.AllUserReady.Size = new System.Drawing.Size(978, 532);
            this.AllUserReady.TabIndex = 2;
            // 
            // ReadyLayout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AllUserReady);
            this.DoubleBuffered = true;
            this.Name = "ReadyLayout";
            this.Size = new System.Drawing.Size(978, 532);
            this.User1.ResumeLayout(false);
            this.User1.PerformLayout();
            this.User2.ResumeLayout(false);
            this.User2.PerformLayout();
            this.User3.ResumeLayout(false);
            this.User3.PerformLayout();
            this.User4.ResumeLayout(false);
            this.User4.PerformLayout();
            this.User5.ResumeLayout(false);
            this.User5.PerformLayout();
            this.User6.ResumeLayout(false);
            this.User6.PerformLayout();
            this.AllUserReady.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel User1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label Status1;
        private System.Windows.Forms.Label FullName1;
        private System.Windows.Forms.Label UserName1;
        private System.Windows.Forms.Panel User2;
        private System.Windows.Forms.Label Status2;
        private System.Windows.Forms.Label FullName2;
        private System.Windows.Forms.Label UserName2;
        private System.Windows.Forms.Panel User3;
        private System.Windows.Forms.Label Status3;
        private System.Windows.Forms.Label FullName3;
        private System.Windows.Forms.Label UserName3;
        private System.Windows.Forms.Panel User4;
        private System.Windows.Forms.Label Status4;
        private System.Windows.Forms.Label FullName4;
        private System.Windows.Forms.Label UserName4;
        private System.Windows.Forms.Panel User5;
        private System.Windows.Forms.Label Status5;
        private System.Windows.Forms.Label FullName5;
        private System.Windows.Forms.Label UserName5;
        private System.Windows.Forms.Panel User6;
        private System.Windows.Forms.Label Status6;
        private System.Windows.Forms.Label FullName6;
        private System.Windows.Forms.Label UserName6;
        private System.Windows.Forms.Button bntReady;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel AllUserReady;
    }
}
