﻿namespace Player.Layout
{
    partial class LoginExam
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtExamPassWord = new System.Windows.Forms.TextBox();
            this.txtExamName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.JoinExam = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtExamPassWord
            // 
            this.txtExamPassWord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtExamPassWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExamPassWord.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.txtExamPassWord.Location = new System.Drawing.Point(5, 6);
            this.txtExamPassWord.Multiline = true;
            this.txtExamPassWord.Name = "txtExamPassWord";
            this.txtExamPassWord.Size = new System.Drawing.Size(254, 25);
            this.txtExamPassWord.TabIndex = 2;
            this.txtExamPassWord.Text = "Mật khẩu";
            this.txtExamPassWord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtExamPassWord.Enter += new System.EventHandler(this.Login_Txt_PassWord_Enter);
            this.txtExamPassWord.Leave += new System.EventHandler(this.Login_Txt_PassWord_Leave);
            // 
            // txtExamName
            // 
            this.txtExamName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtExamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExamName.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.txtExamName.Location = new System.Drawing.Point(6, 6);
            this.txtExamName.Multiline = true;
            this.txtExamName.Name = "txtExamName";
            this.txtExamName.Size = new System.Drawing.Size(254, 25);
            this.txtExamName.TabIndex = 1;
            this.txtExamName.Text = "Mã cuộc thi";
            this.txtExamName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtExamName.Enter += new System.EventHandler(this.Login_Txt_UserName_Enter);
            this.txtExamName.Leave += new System.EventHandler(this.Login_Txt_UserName_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtExamName);
            this.panel2.Location = new System.Drawing.Point(372, 259);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(265, 40);
            this.panel2.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.GhostWhite;
            this.panel1.Controls.Add(this.txtExamPassWord);
            this.panel1.Location = new System.Drawing.Point(372, 311);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 40);
            this.panel1.TabIndex = 9;
            // 
            // JoinExam
            // 
            this.JoinExam.BackColor = System.Drawing.Color.Transparent;
            this.JoinExam.Image = global::Player.Properties.Resources.Button_Play_icon1;
            this.JoinExam.Location = new System.Drawing.Point(916, 476);
            this.JoinExam.Name = "JoinExam";
            this.JoinExam.Size = new System.Drawing.Size(59, 56);
            this.JoinExam.TabIndex = 3;
            this.toolTip1.SetToolTip(this.JoinExam, "Tham Gia");
            this.JoinExam.Click += new System.EventHandler(this.button1_Click);
            // 
            // LoginExam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Player.Properties.Resources.anh_du_thi_so_6_ha_long_ve_dem__4_;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.JoinExam);
            this.DoubleBuffered = true;
            this.Name = "LoginExam";
            this.Size = new System.Drawing.Size(978, 532);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtExamPassWord;
        private System.Windows.Forms.TextBox txtExamName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label JoinExam;
    }
}
