﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Player.Custom;
using Player.Network;
using Player.Properties;

namespace Player.Layout
{
    public partial class ReadyLayout : UserControl
    {
        private readonly Action _callback;
        public ReadyLayout(Action callback)
        {
            InitializeComponent();
            BroadcastFunction.OnResLoginExam = OnBroadcastLoginExam;
            BroadcastFunction.OnResBeginExam = OnBroadcastBeginExam;
            _callback = callback;
        }

        private void OnBroadcastBeginExam(string data)
        {

            BroadcastExamBegin broadcastExamBegin = NetworkUtil.ParseJson<BroadcastExamBegin>(data);
            if (broadcastExamBegin.StateExam == (int)State.ExamBegin)
            {
                _callback();
            }
        }

        private void OnBroadcastLoginExam(string data)
        {
            BroadcastLoginExam broadcastLoginExam = NetworkUtil.ParseJson<BroadcastLoginExam>(data);
            foreach (Control control in AllUserReady.Controls)
            {
                if (control is Panel)
                {
                    BeginInvoke((Action)(() =>
                    {
                        control.Visible = false;
                    }));
                }
            }
            for (int i = 0; i < broadcastLoginExam.ListUserState.Count; i++)
            {
                BeginInvoke((Action)(() =>
               {
                   bntReady.Text = DefaultValue.MyUser.Role == (int)Role.User ? Resources.Ready : Resources.Start;
               }));

                UserState userState = broadcastLoginExam.ListUserState[i];
                if (userState.State == (int)State.Start) continue;
                Panel userPanel = AllUserReady.Controls.Find("User" + (i + 1), true).FirstOrDefault() as Panel;
                if (userPanel != null)
                {
                    var i1 = i;
                    BeginInvoke((Action)(() =>
                    {
                        userPanel.Visible = true;
                        var userName = userPanel.Controls.Find("UserName" + (i1 + 1), true).FirstOrDefault() as Label;
                        if (userName != null)
                            userName.Text = userState.User.UserName;
                        var fullName = userPanel.Controls.Find("FullName" + (i1 + 1), true).FirstOrDefault() as Label;
                        if (fullName != null)
                            fullName.Text = userState.User.FullName;
                        var status = userPanel.Controls.Find("Status" + (i1 + 1), true).FirstOrDefault() as Label;
                        if (status != null)
                        {
                            if (userState.State == (int)State.NotReady)
                            {
                                status.Text = Resources.NotReady;
                                status.ForeColor = Color.Red;
                            }
                            else
                            {
                                status.Text = Resources.Ready;
                                status.ForeColor = Color.LimeGreen;
                            }
                        }

                    }));
                }
            }
        }

        private void bntReady_Click(object sender, EventArgs e)
        {
            StateChangeExamReq stateChangeExam = new StateChangeExamReq
            {
                ExamId = DefaultValue.ExamId,
                RequestName = (int)RequestName.StateChangeExam
            };
            NetworkMain.Send(stateChangeExam, OnResReadyClick);
        }

        private void OnResReadyClick(string data)
        {
        }

    }
}
