﻿namespace Player.Layout
{
    partial class PlayExam
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayExam));
            this.PointPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.PointPanelChild1 = new System.Windows.Forms.Panel();
            this.AnswerOutput1 = new System.Windows.Forms.TextBox();
            this.Fail1 = new System.Windows.Forms.Button();
            this.Right1 = new System.Windows.Forms.Button();
            this.UserNameLabel1 = new System.Windows.Forms.Label();
            this.PointLabel1 = new System.Windows.Forms.Label();
            this.PointPanelChild2 = new System.Windows.Forms.Panel();
            this.AnswerOutput2 = new System.Windows.Forms.TextBox();
            this.Fail2 = new System.Windows.Forms.Button();
            this.Right2 = new System.Windows.Forms.Button();
            this.UserNameLabel2 = new System.Windows.Forms.Label();
            this.PointLabel2 = new System.Windows.Forms.Label();
            this.PointPanelChild3 = new System.Windows.Forms.Panel();
            this.AnswerOutput3 = new System.Windows.Forms.TextBox();
            this.Fail3 = new System.Windows.Forms.Button();
            this.Right3 = new System.Windows.Forms.Button();
            this.UserNameLabel3 = new System.Windows.Forms.Label();
            this.PointLabel3 = new System.Windows.Forms.Label();
            this.PointPanelChild4 = new System.Windows.Forms.Panel();
            this.AnswerOutput4 = new System.Windows.Forms.TextBox();
            this.Fail4 = new System.Windows.Forms.Button();
            this.Right4 = new System.Windows.Forms.Button();
            this.UserNameLabel4 = new System.Windows.Forms.Label();
            this.PointLabel4 = new System.Windows.Forms.Label();
            this.TimeExamLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.TitleQuestion = new System.Windows.Forms.Label();
            this.ButtonSubmit = new System.Windows.Forms.Button();
            this.AnswerInput = new System.Windows.Forms.TextBox();
            this.AnswerPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.answerChoice1 = new System.Windows.Forms.Label();
            this.answerChoice2 = new System.Windows.Forms.Label();
            this.answerChoice3 = new System.Windows.Forms.Label();
            this.answerChoice4 = new System.Windows.Forms.Label();
            this.answerChoice5 = new System.Windows.Forms.Label();
            this.answerChoice6 = new System.Windows.Forms.Label();
            this.answerChoice7 = new System.Windows.Forms.Label();
            this.answerChoice9 = new System.Windows.Forms.Label();
            this.QuestionImage = new System.Windows.Forms.Panel();
            this.ViewAnswer = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_NexPlayer = new System.Windows.Forms.Button();
            this.PointPanel.SuspendLayout();
            this.PointPanelChild1.SuspendLayout();
            this.PointPanelChild2.SuspendLayout();
            this.PointPanelChild3.SuspendLayout();
            this.PointPanelChild4.SuspendLayout();
            this.AnswerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PointPanel
            // 
            this.PointPanel.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.PointPanel.Controls.Add(this.PointPanelChild1);
            this.PointPanel.Controls.Add(this.PointPanelChild2);
            this.PointPanel.Controls.Add(this.PointPanelChild3);
            this.PointPanel.Controls.Add(this.PointPanelChild4);
            this.PointPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.PointPanel.Location = new System.Drawing.Point(796, 0);
            this.PointPanel.Name = "PointPanel";
            this.PointPanel.Size = new System.Drawing.Size(182, 532);
            this.PointPanel.TabIndex = 0;
            // 
            // PointPanelChild1
            // 
            this.PointPanelChild1.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.PointPanelChild1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PointPanelChild1.Controls.Add(this.AnswerOutput1);
            this.PointPanelChild1.Controls.Add(this.Fail1);
            this.PointPanelChild1.Controls.Add(this.Right1);
            this.PointPanelChild1.Controls.Add(this.UserNameLabel1);
            this.PointPanelChild1.Controls.Add(this.PointLabel1);
            this.PointPanelChild1.Location = new System.Drawing.Point(3, 3);
            this.PointPanelChild1.Name = "PointPanelChild1";
            this.PointPanelChild1.Size = new System.Drawing.Size(176, 125);
            this.PointPanelChild1.TabIndex = 2;
            this.PointPanelChild1.Visible = false;
            // 
            // AnswerOutput1
            // 
            this.AnswerOutput1.Location = new System.Drawing.Point(10, 35);
            this.AnswerOutput1.Multiline = true;
            this.AnswerOutput1.Name = "AnswerOutput1";
            this.AnswerOutput1.ReadOnly = true;
            this.AnswerOutput1.Size = new System.Drawing.Size(155, 52);
            this.AnswerOutput1.TabIndex = 4;
            // 
            // Fail1
            // 
            this.Fail1.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Fail1.BackgroundImage = global::Player.Properties.Resources.Action_remove_icon;
            this.Fail1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Fail1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Fail1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Fail1.ForeColor = System.Drawing.Color.White;
            this.Fail1.Location = new System.Drawing.Point(131, 92);
            this.Fail1.Name = "Fail1";
            this.Fail1.Size = new System.Drawing.Size(32, 23);
            this.Fail1.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Fail1, "Trừ điểm");
            this.Fail1.UseVisualStyleBackColor = false;
            this.Fail1.Visible = false;
            this.Fail1.Click += new System.EventHandler(this.Left_Click);
            // 
            // Right1
            // 
            this.Right1.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Right1.BackgroundImage = global::Player.Properties.Resources.add_icon;
            this.Right1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Right1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Right1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Right1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Right1.ForeColor = System.Drawing.Color.White;
            this.Right1.Location = new System.Drawing.Point(93, 92);
            this.Right1.Name = "Right1";
            this.Right1.Size = new System.Drawing.Size(32, 23);
            this.Right1.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Right1, "Cộng điểm");
            this.Right1.UseVisualStyleBackColor = false;
            this.Right1.Visible = false;
            this.Right1.Click += new System.EventHandler(this.Right_Click);
            // 
            // UserNameLabel1
            // 
            this.UserNameLabel1.AutoSize = true;
            this.UserNameLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLabel1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.UserNameLabel1.Location = new System.Drawing.Point(53, 10);
            this.UserNameLabel1.Name = "UserNameLabel1";
            this.UserNameLabel1.Size = new System.Drawing.Size(108, 16);
            this.UserNameLabel1.TabIndex = 1;
            this.UserNameLabel1.Text = "Nguyễn Đức Huy";
            // 
            // PointLabel1
            // 
            this.PointLabel1.BackColor = System.Drawing.Color.Transparent;
            this.PointLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointLabel1.ForeColor = System.Drawing.Color.Red;
            this.PointLabel1.Location = new System.Drawing.Point(4, 4);
            this.PointLabel1.Name = "PointLabel1";
            this.PointLabel1.Size = new System.Drawing.Size(47, 31);
            this.PointLabel1.TabIndex = 1;
            this.PointLabel1.Text = "120";
            this.PointLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PointPanelChild2
            // 
            this.PointPanelChild2.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.PointPanelChild2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PointPanelChild2.Controls.Add(this.AnswerOutput2);
            this.PointPanelChild2.Controls.Add(this.Fail2);
            this.PointPanelChild2.Controls.Add(this.Right2);
            this.PointPanelChild2.Controls.Add(this.UserNameLabel2);
            this.PointPanelChild2.Controls.Add(this.PointLabel2);
            this.PointPanelChild2.Location = new System.Drawing.Point(3, 134);
            this.PointPanelChild2.Name = "PointPanelChild2";
            this.PointPanelChild2.Size = new System.Drawing.Size(176, 125);
            this.PointPanelChild2.TabIndex = 2;
            this.PointPanelChild2.Visible = false;
            // 
            // AnswerOutput2
            // 
            this.AnswerOutput2.Location = new System.Drawing.Point(8, 36);
            this.AnswerOutput2.Multiline = true;
            this.AnswerOutput2.Name = "AnswerOutput2";
            this.AnswerOutput2.ReadOnly = true;
            this.AnswerOutput2.Size = new System.Drawing.Size(155, 52);
            this.AnswerOutput2.TabIndex = 4;
            // 
            // Fail2
            // 
            this.Fail2.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Fail2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Fail2.BackgroundImage")));
            this.Fail2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Fail2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Fail2.ForeColor = System.Drawing.Color.White;
            this.Fail2.Location = new System.Drawing.Point(131, 92);
            this.Fail2.Name = "Fail2";
            this.Fail2.Size = new System.Drawing.Size(32, 23);
            this.Fail2.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Fail2, "Trừ điểm");
            this.Fail2.UseVisualStyleBackColor = false;
            this.Fail2.Visible = false;
            this.Fail2.Click += new System.EventHandler(this.Left_Click);
            // 
            // Right2
            // 
            this.Right2.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Right2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Right2.BackgroundImage")));
            this.Right2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Right2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Right2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Right2.ForeColor = System.Drawing.SystemColors.Control;
            this.Right2.Location = new System.Drawing.Point(93, 92);
            this.Right2.Name = "Right2";
            this.Right2.Size = new System.Drawing.Size(32, 23);
            this.Right2.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Right2, "Cộng điểm");
            this.Right2.UseVisualStyleBackColor = false;
            this.Right2.Visible = false;
            this.Right2.Click += new System.EventHandler(this.Right_Click);
            // 
            // UserNameLabel2
            // 
            this.UserNameLabel2.AutoSize = true;
            this.UserNameLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLabel2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.UserNameLabel2.Location = new System.Drawing.Point(53, 12);
            this.UserNameLabel2.Name = "UserNameLabel2";
            this.UserNameLabel2.Size = new System.Drawing.Size(108, 16);
            this.UserNameLabel2.TabIndex = 1;
            this.UserNameLabel2.Text = "Nguyễn Đức Huy";
            // 
            // PointLabel2
            // 
            this.PointLabel2.BackColor = System.Drawing.Color.Transparent;
            this.PointLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointLabel2.ForeColor = System.Drawing.Color.Red;
            this.PointLabel2.Location = new System.Drawing.Point(5, 5);
            this.PointLabel2.Name = "PointLabel2";
            this.PointLabel2.Size = new System.Drawing.Size(47, 31);
            this.PointLabel2.TabIndex = 1;
            this.PointLabel2.Text = "120";
            this.PointLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PointPanelChild3
            // 
            this.PointPanelChild3.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.PointPanelChild3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PointPanelChild3.Controls.Add(this.AnswerOutput3);
            this.PointPanelChild3.Controls.Add(this.Fail3);
            this.PointPanelChild3.Controls.Add(this.Right3);
            this.PointPanelChild3.Controls.Add(this.UserNameLabel3);
            this.PointPanelChild3.Controls.Add(this.PointLabel3);
            this.PointPanelChild3.Location = new System.Drawing.Point(3, 265);
            this.PointPanelChild3.Name = "PointPanelChild3";
            this.PointPanelChild3.Size = new System.Drawing.Size(176, 125);
            this.PointPanelChild3.TabIndex = 3;
            this.PointPanelChild3.Visible = false;
            // 
            // AnswerOutput3
            // 
            this.AnswerOutput3.Location = new System.Drawing.Point(10, 37);
            this.AnswerOutput3.Multiline = true;
            this.AnswerOutput3.Name = "AnswerOutput3";
            this.AnswerOutput3.ReadOnly = true;
            this.AnswerOutput3.Size = new System.Drawing.Size(155, 52);
            this.AnswerOutput3.TabIndex = 4;
            // 
            // Fail3
            // 
            this.Fail3.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Fail3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Fail3.BackgroundImage")));
            this.Fail3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Fail3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Fail3.ForeColor = System.Drawing.Color.White;
            this.Fail3.Location = new System.Drawing.Point(131, 95);
            this.Fail3.Name = "Fail3";
            this.Fail3.Size = new System.Drawing.Size(32, 23);
            this.Fail3.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Fail3, "Trừ điểm");
            this.Fail3.UseVisualStyleBackColor = false;
            this.Fail3.Visible = false;
            this.Fail3.Click += new System.EventHandler(this.Left_Click);
            // 
            // Right3
            // 
            this.Right3.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Right3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Right3.BackgroundImage")));
            this.Right3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Right3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Right3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Right3.ForeColor = System.Drawing.SystemColors.Control;
            this.Right3.Location = new System.Drawing.Point(93, 95);
            this.Right3.Name = "Right3";
            this.Right3.Size = new System.Drawing.Size(32, 23);
            this.Right3.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Right3, "Cộng điểm");
            this.Right3.UseVisualStyleBackColor = false;
            this.Right3.Visible = false;
            this.Right3.Click += new System.EventHandler(this.Right_Click);
            // 
            // UserNameLabel3
            // 
            this.UserNameLabel3.AutoSize = true;
            this.UserNameLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLabel3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.UserNameLabel3.Location = new System.Drawing.Point(55, 12);
            this.UserNameLabel3.Name = "UserNameLabel3";
            this.UserNameLabel3.Size = new System.Drawing.Size(108, 16);
            this.UserNameLabel3.TabIndex = 1;
            this.UserNameLabel3.Text = "Nguyễn Đức Huy";
            // 
            // PointLabel3
            // 
            this.PointLabel3.BackColor = System.Drawing.Color.Transparent;
            this.PointLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointLabel3.ForeColor = System.Drawing.Color.Red;
            this.PointLabel3.Location = new System.Drawing.Point(5, 5);
            this.PointLabel3.Name = "PointLabel3";
            this.PointLabel3.Size = new System.Drawing.Size(47, 31);
            this.PointLabel3.TabIndex = 1;
            this.PointLabel3.Text = "120";
            this.PointLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PointPanelChild4
            // 
            this.PointPanelChild4.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.PointPanelChild4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PointPanelChild4.Controls.Add(this.AnswerOutput4);
            this.PointPanelChild4.Controls.Add(this.Fail4);
            this.PointPanelChild4.Controls.Add(this.Right4);
            this.PointPanelChild4.Controls.Add(this.UserNameLabel4);
            this.PointPanelChild4.Controls.Add(this.PointLabel4);
            this.PointPanelChild4.Location = new System.Drawing.Point(3, 396);
            this.PointPanelChild4.Name = "PointPanelChild4";
            this.PointPanelChild4.Size = new System.Drawing.Size(176, 125);
            this.PointPanelChild4.TabIndex = 4;
            this.PointPanelChild4.Visible = false;
            // 
            // AnswerOutput4
            // 
            this.AnswerOutput4.Location = new System.Drawing.Point(9, 38);
            this.AnswerOutput4.Multiline = true;
            this.AnswerOutput4.Name = "AnswerOutput4";
            this.AnswerOutput4.ReadOnly = true;
            this.AnswerOutput4.Size = new System.Drawing.Size(155, 52);
            this.AnswerOutput4.TabIndex = 4;
            // 
            // Fail4
            // 
            this.Fail4.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Fail4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Fail4.BackgroundImage")));
            this.Fail4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Fail4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Fail4.ForeColor = System.Drawing.Color.White;
            this.Fail4.Location = new System.Drawing.Point(131, 96);
            this.Fail4.Name = "Fail4";
            this.Fail4.Size = new System.Drawing.Size(32, 23);
            this.Fail4.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Fail4, "Trừ điểm");
            this.Fail4.UseVisualStyleBackColor = false;
            this.Fail4.Visible = false;
            this.Fail4.Click += new System.EventHandler(this.Left_Click);
            // 
            // Right4
            // 
            this.Right4.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.Right4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Right4.BackgroundImage")));
            this.Right4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Right4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Right4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Right4.ForeColor = System.Drawing.SystemColors.Control;
            this.Right4.Location = new System.Drawing.Point(93, 96);
            this.Right4.Name = "Right4";
            this.Right4.Size = new System.Drawing.Size(32, 23);
            this.Right4.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Right4, "Cộng điểm");
            this.Right4.UseVisualStyleBackColor = false;
            this.Right4.Visible = false;
            this.Right4.Click += new System.EventHandler(this.Right_Click);
            // 
            // UserNameLabel4
            // 
            this.UserNameLabel4.AutoSize = true;
            this.UserNameLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLabel4.ForeColor = System.Drawing.SystemColors.Highlight;
            this.UserNameLabel4.Location = new System.Drawing.Point(55, 12);
            this.UserNameLabel4.Name = "UserNameLabel4";
            this.UserNameLabel4.Size = new System.Drawing.Size(108, 16);
            this.UserNameLabel4.TabIndex = 1;
            this.UserNameLabel4.Text = "Nguyễn Đức Huy";
            // 
            // PointLabel4
            // 
            this.PointLabel4.BackColor = System.Drawing.Color.Transparent;
            this.PointLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointLabel4.ForeColor = System.Drawing.Color.Red;
            this.PointLabel4.Location = new System.Drawing.Point(6, 6);
            this.PointLabel4.Name = "PointLabel4";
            this.PointLabel4.Size = new System.Drawing.Size(47, 31);
            this.PointLabel4.TabIndex = 1;
            this.PointLabel4.Text = "120";
            this.PointLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TimeExamLabel
            // 
            this.TimeExamLabel.AutoSize = true;
            this.TimeExamLabel.BackColor = System.Drawing.Color.Transparent;
            this.TimeExamLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeExamLabel.ForeColor = System.Drawing.Color.Aqua;
            this.TimeExamLabel.Location = new System.Drawing.Point(685, 31);
            this.TimeExamLabel.Name = "TimeExamLabel";
            this.TimeExamLabel.Size = new System.Drawing.Size(51, 25);
            this.TimeExamLabel.TabIndex = 2;
            this.TimeExamLabel.Text = "200";
            this.TimeExamLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TimeExamLabel.Click += new System.EventHandler(this.TimeExamLabel_Click);
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Image = global::Player.Properties.Resources.Clock_icon;
            this.label16.Location = new System.Drawing.Point(737, 25);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 36);
            this.label16.TabIndex = 2;
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.label16, "Thời gian còn lại");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.LawnGreen;
            this.label17.Location = new System.Drawing.Point(22, 32);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(198, 22);
            this.label17.TabIndex = 2;
            this.label17.Text = "Phần Thi Khởi Động";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.BackColor = System.Drawing.Color.Transparent;
            this.QuestionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionLabel.ForeColor = System.Drawing.Color.White;
            this.QuestionLabel.Location = new System.Drawing.Point(61, 69);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(729, 80);
            this.QuestionLabel.TabIndex = 4;
            this.QuestionLabel.Text = "Ai là vị vua đầu tiên của Trung Quốc?";
            this.QuestionLabel.TextChanged += new System.EventHandler(this.QuestionLabel_TextChanged);
            // 
            // TitleQuestion
            // 
            this.TitleQuestion.BackColor = System.Drawing.Color.Transparent;
            this.TitleQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleQuestion.ForeColor = System.Drawing.Color.White;
            this.TitleQuestion.Image = global::Player.Properties.Resources.Button_Help_icon;
            this.TitleQuestion.Location = new System.Drawing.Point(23, 66);
            this.TitleQuestion.Name = "TitleQuestion";
            this.TitleQuestion.Size = new System.Drawing.Size(32, 26);
            this.TitleQuestion.TabIndex = 5;
            this.TitleQuestion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TitleQuestion.Click += new System.EventHandler(this.TitleQuestion_Click);
            // 
            // ButtonSubmit
            // 
            this.ButtonSubmit.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSubmit.BackgroundImage = global::Player.Properties.Resources.hands_ok_icon__1_;
            this.ButtonSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonSubmit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSubmit.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ButtonSubmit.Location = new System.Drawing.Point(473, 482);
            this.ButtonSubmit.Name = "ButtonSubmit";
            this.ButtonSubmit.Size = new System.Drawing.Size(39, 37);
            this.ButtonSubmit.TabIndex = 8;
            this.toolTip1.SetToolTip(this.ButtonSubmit, "Trả lời");
            this.ButtonSubmit.UseVisualStyleBackColor = false;
            this.ButtonSubmit.Click += new System.EventHandler(this.ButtonSubmit_Click);
            // 
            // AnswerInput
            // 
            this.AnswerInput.BackColor = System.Drawing.Color.White;
            this.AnswerInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AnswerInput.ForeColor = System.Drawing.Color.DarkGreen;
            this.AnswerInput.Location = new System.Drawing.Point(520, 487);
            this.AnswerInput.Name = "AnswerInput";
            this.AnswerInput.Size = new System.Drawing.Size(262, 26);
            this.AnswerInput.TabIndex = 7;
            this.AnswerInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AnswerPanel
            // 
            this.AnswerPanel.BackColor = System.Drawing.Color.Transparent;
            this.AnswerPanel.Controls.Add(this.answerChoice1);
            this.AnswerPanel.Controls.Add(this.answerChoice2);
            this.AnswerPanel.Controls.Add(this.answerChoice3);
            this.AnswerPanel.Controls.Add(this.answerChoice4);
            this.AnswerPanel.Controls.Add(this.answerChoice5);
            this.AnswerPanel.Controls.Add(this.answerChoice6);
            this.AnswerPanel.Controls.Add(this.answerChoice7);
            this.AnswerPanel.Controls.Add(this.answerChoice9);
            this.AnswerPanel.Location = new System.Drawing.Point(23, 152);
            this.AnswerPanel.Name = "AnswerPanel";
            this.AnswerPanel.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.AnswerPanel.Size = new System.Drawing.Size(233, 317);
            this.AnswerPanel.TabIndex = 9;
            // 
            // answerChoice1
            // 
            this.answerChoice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice1.ForeColor = System.Drawing.Color.White;
            this.answerChoice1.Location = new System.Drawing.Point(3, 13);
            this.answerChoice1.Margin = new System.Windows.Forms.Padding(3);
            this.answerChoice1.Name = "answerChoice1";
            this.answerChoice1.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice1.Size = new System.Drawing.Size(217, 23);
            this.answerChoice1.TabIndex = 0;
            this.answerChoice1.Text = "A : Minh Mạng";
            this.answerChoice1.Visible = false;
            // 
            // answerChoice2
            // 
            this.answerChoice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice2.ForeColor = System.Drawing.Color.White;
            this.answerChoice2.Location = new System.Drawing.Point(3, 39);
            this.answerChoice2.Name = "answerChoice2";
            this.answerChoice2.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice2.Size = new System.Drawing.Size(217, 23);
            this.answerChoice2.TabIndex = 0;
            this.answerChoice2.Text = "B : Càn Long";
            this.answerChoice2.Visible = false;
            // 
            // answerChoice3
            // 
            this.answerChoice3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice3.ForeColor = System.Drawing.Color.White;
            this.answerChoice3.Location = new System.Drawing.Point(3, 62);
            this.answerChoice3.Name = "answerChoice3";
            this.answerChoice3.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice3.Size = new System.Drawing.Size(217, 23);
            this.answerChoice3.TabIndex = 0;
            this.answerChoice3.Text = "C : Quang Tự";
            this.answerChoice3.Visible = false;
            // 
            // answerChoice4
            // 
            this.answerChoice4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice4.ForeColor = System.Drawing.Color.White;
            this.answerChoice4.Location = new System.Drawing.Point(3, 85);
            this.answerChoice4.Name = "answerChoice4";
            this.answerChoice4.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice4.Size = new System.Drawing.Size(217, 23);
            this.answerChoice4.TabIndex = 0;
            this.answerChoice4.Text = "D : Triều Nhai";
            this.answerChoice4.Visible = false;
            // 
            // answerChoice5
            // 
            this.answerChoice5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice5.ForeColor = System.Drawing.Color.White;
            this.answerChoice5.Location = new System.Drawing.Point(3, 108);
            this.answerChoice5.Name = "answerChoice5";
            this.answerChoice5.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice5.Size = new System.Drawing.Size(217, 23);
            this.answerChoice5.TabIndex = 1;
            this.answerChoice5.Text = "D : Triều Nhai";
            this.answerChoice5.Visible = false;
            // 
            // answerChoice6
            // 
            this.answerChoice6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice6.ForeColor = System.Drawing.Color.White;
            this.answerChoice6.Location = new System.Drawing.Point(3, 131);
            this.answerChoice6.Name = "answerChoice6";
            this.answerChoice6.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice6.Size = new System.Drawing.Size(217, 23);
            this.answerChoice6.TabIndex = 2;
            this.answerChoice6.Text = "D : Triều Nhai";
            this.answerChoice6.Visible = false;
            // 
            // answerChoice7
            // 
            this.answerChoice7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice7.ForeColor = System.Drawing.Color.White;
            this.answerChoice7.Location = new System.Drawing.Point(3, 154);
            this.answerChoice7.Name = "answerChoice7";
            this.answerChoice7.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice7.Size = new System.Drawing.Size(217, 23);
            this.answerChoice7.TabIndex = 3;
            this.answerChoice7.Text = "D : Triều Nhai";
            this.answerChoice7.Visible = false;
            // 
            // answerChoice9
            // 
            this.answerChoice9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerChoice9.ForeColor = System.Drawing.Color.White;
            this.answerChoice9.Location = new System.Drawing.Point(3, 177);
            this.answerChoice9.Name = "answerChoice9";
            this.answerChoice9.Padding = new System.Windows.Forms.Padding(17, 0, 0, 0);
            this.answerChoice9.Size = new System.Drawing.Size(217, 23);
            this.answerChoice9.TabIndex = 4;
            this.answerChoice9.Text = "D : Triều Nhai";
            this.answerChoice9.Visible = false;
            // 
            // QuestionImage
            // 
            this.QuestionImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.QuestionImage.Location = new System.Drawing.Point(264, 152);
            this.QuestionImage.Name = "QuestionImage";
            this.QuestionImage.Size = new System.Drawing.Size(518, 317);
            this.QuestionImage.TabIndex = 10;
            // 
            // ViewAnswer
            // 
            this.ViewAnswer.BackColor = System.Drawing.Color.Transparent;
            this.ViewAnswer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ViewAnswer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ViewAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewAnswer.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.ViewAnswer.Image = global::Player.Properties.Resources.help_desk_icon__1_;
            this.ViewAnswer.Location = new System.Drawing.Point(23, 484);
            this.ViewAnswer.Name = "ViewAnswer";
            this.ViewAnswer.Size = new System.Drawing.Size(33, 35);
            this.ViewAnswer.TabIndex = 11;
            this.toolTip1.SetToolTip(this.ViewAnswer, "Đáp Án");
            this.ViewAnswer.UseVisualStyleBackColor = false;
            this.ViewAnswer.Visible = false;
            this.ViewAnswer.Click += new System.EventHandler(this.ViewAnswer_Click);
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.Transparent;
            this.btnNext.BackgroundImage = global::Player.Properties.Resources.Button_Next_icon;
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnNext.Location = new System.Drawing.Point(62, 484);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(38, 35);
            this.btnNext.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btnNext, "Tiếp Tục");
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btn_NexPlayer
            // 
            this.btn_NexPlayer.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_NexPlayer.Location = new System.Drawing.Point(159, 482);
            this.btn_NexPlayer.Name = "btn_NexPlayer";
            this.btn_NexPlayer.Size = new System.Drawing.Size(92, 35);
            this.btn_NexPlayer.TabIndex = 12;
            this.btn_NexPlayer.Text = "Tiếp tục";
            this.toolTip1.SetToolTip(this.btn_NexPlayer, "Người chơi tiếp theo");
            this.btn_NexPlayer.UseVisualStyleBackColor = false;
            this.btn_NexPlayer.Visible = false;
            // 
            // PlayExam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Player.Properties.Resources.dark_red_brick_wall_texture;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.btn_NexPlayer);
            this.Controls.Add(this.AnswerInput);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.ViewAnswer);
            this.Controls.Add(this.AnswerPanel);
            this.Controls.Add(this.QuestionImage);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.TitleQuestion);
            this.Controls.Add(this.ButtonSubmit);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.TimeExamLabel);
            this.Controls.Add(this.PointPanel);
            this.DoubleBuffered = true;
            this.Name = "PlayExam";
            this.Size = new System.Drawing.Size(978, 532);
            this.toolTip1.SetToolTip(this, "Câu hỏi");
            this.PointPanel.ResumeLayout(false);
            this.PointPanelChild1.ResumeLayout(false);
            this.PointPanelChild1.PerformLayout();
            this.PointPanelChild2.ResumeLayout(false);
            this.PointPanelChild2.PerformLayout();
            this.PointPanelChild3.ResumeLayout(false);
            this.PointPanelChild3.PerformLayout();
            this.PointPanelChild4.ResumeLayout(false);
            this.PointPanelChild4.PerformLayout();
            this.AnswerPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel PointPanel;
        private System.Windows.Forms.Label PointLabel1;
        private System.Windows.Forms.Panel PointPanelChild1;
        private System.Windows.Forms.Button Fail1;
        private System.Windows.Forms.Button Right1;
        private System.Windows.Forms.Label UserNameLabel1;
        private System.Windows.Forms.Panel PointPanelChild2;
        private System.Windows.Forms.Button Fail2;
        private System.Windows.Forms.Button Right2;
        private System.Windows.Forms.Label UserNameLabel2;
        private System.Windows.Forms.Label PointLabel2;
        private System.Windows.Forms.Panel PointPanelChild3;
        private System.Windows.Forms.Button Fail3;
        private System.Windows.Forms.Button Right3;
        private System.Windows.Forms.Label UserNameLabel3;
        private System.Windows.Forms.Label PointLabel3;
        private System.Windows.Forms.Panel PointPanelChild4;
        private System.Windows.Forms.Button Fail4;
        private System.Windows.Forms.Button Right4;
        private System.Windows.Forms.Label UserNameLabel4;
        private System.Windows.Forms.Label PointLabel4;
        private System.Windows.Forms.Label TimeExamLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox AnswerOutput1;
        private System.Windows.Forms.TextBox AnswerOutput2;
        private System.Windows.Forms.TextBox AnswerOutput3;
        private System.Windows.Forms.TextBox AnswerOutput4;
        private System.Windows.Forms.Label QuestionLabel;
        private System.Windows.Forms.Label TitleQuestion;
        private System.Windows.Forms.Button ButtonSubmit;
        private System.Windows.Forms.TextBox AnswerInput;
        private System.Windows.Forms.FlowLayoutPanel AnswerPanel;
        private System.Windows.Forms.Label answerChoice1;
        private System.Windows.Forms.Label answerChoice2;
        private System.Windows.Forms.Label answerChoice3;
        private System.Windows.Forms.Label answerChoice4;
        private System.Windows.Forms.Label answerChoice5;
        private System.Windows.Forms.Label answerChoice6;
        private System.Windows.Forms.Label answerChoice7;
        private System.Windows.Forms.Label answerChoice9;
        private System.Windows.Forms.Panel QuestionImage;
        private System.Windows.Forms.Button ViewAnswer;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btn_NexPlayer;
    }
}
