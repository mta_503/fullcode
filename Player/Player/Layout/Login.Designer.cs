﻿namespace Player.Layout
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Login_Btn_Login = new System.Windows.Forms.Button();
            this.Login_Btn_Exit = new System.Windows.Forms.Button();
            this.extendedPanel1 = new Player.Layout.ExtendedPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Login_Txt_PassWord = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Login_Txt_UserName = new System.Windows.Forms.TextBox();
            this.extendedPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Login_Btn_Login
            // 
            this.Login_Btn_Login.BackColor = System.Drawing.Color.Transparent;
            this.Login_Btn_Login.BackgroundImage = global::Player.Properties.Resources.gnome_session_logout;
            this.Login_Btn_Login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Login_Btn_Login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Login_Btn_Login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Login_Btn_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Btn_Login.Location = new System.Drawing.Point(237, 164);
            this.Login_Btn_Login.Name = "Login_Btn_Login";
            this.Login_Btn_Login.Size = new System.Drawing.Size(40, 40);
            this.Login_Btn_Login.TabIndex = 3;
            this.toolTip1.SetToolTip(this.Login_Btn_Login, "Đăng Nhập");
            this.Login_Btn_Login.UseVisualStyleBackColor = false;
            this.Login_Btn_Login.Click += new System.EventHandler(this.Login_Btn_Login_Click);
            // 
            // Login_Btn_Exit
            // 
            this.Login_Btn_Exit.BackColor = System.Drawing.Color.Transparent;
            this.Login_Btn_Exit.BackgroundImage = global::Player.Properties.Resources.Log_Out;
            this.Login_Btn_Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Login_Btn_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Login_Btn_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Btn_Exit.Location = new System.Drawing.Point(292, 164);
            this.Login_Btn_Exit.Name = "Login_Btn_Exit";
            this.Login_Btn_Exit.Size = new System.Drawing.Size(40, 40);
            this.Login_Btn_Exit.TabIndex = 4;
            this.toolTip1.SetToolTip(this.Login_Btn_Exit, "Thoát");
            this.Login_Btn_Exit.UseVisualStyleBackColor = false;
            // 
            // extendedPanel1
            // 
            this.extendedPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.extendedPanel1.Controls.Add(this.panel2);
            this.extendedPanel1.Controls.Add(this.Login_Btn_Login);
            this.extendedPanel1.Controls.Add(this.panel1);
            this.extendedPanel1.Controls.Add(this.Login_Btn_Exit);
            this.extendedPanel1.Location = new System.Drawing.Point(345, 153);
            this.extendedPanel1.Name = "extendedPanel1";
            this.extendedPanel1.Opacity = 15;
            this.extendedPanel1.Size = new System.Drawing.Size(415, 256);
            this.extendedPanel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.Login_Txt_PassWord);
            this.panel2.Location = new System.Drawing.Point(83, 106);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(249, 40);
            this.panel2.TabIndex = 10;
            // 
            // Login_Txt_PassWord
            // 
            this.Login_Txt_PassWord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Login_Txt_PassWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Txt_PassWord.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.Login_Txt_PassWord.Location = new System.Drawing.Point(6, 9);
            this.Login_Txt_PassWord.Name = "Login_Txt_PassWord";
            this.Login_Txt_PassWord.Size = new System.Drawing.Size(236, 22);
            this.Login_Txt_PassWord.TabIndex = 2;
            this.Login_Txt_PassWord.Text = "PassWord";
            this.Login_Txt_PassWord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Login_Txt_PassWord.Enter += new System.EventHandler(this.Login_Txt_PassWord_Enter);
            this.Login_Txt_PassWord.Leave += new System.EventHandler(this.Login_Txt_PassWord_Leave);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.Login_Txt_UserName);
            this.panel1.Location = new System.Drawing.Point(83, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(249, 40);
            this.panel1.TabIndex = 10;
            // 
            // Login_Txt_UserName
            // 
            this.Login_Txt_UserName.BackColor = System.Drawing.Color.White;
            this.Login_Txt_UserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Login_Txt_UserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Txt_UserName.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.Login_Txt_UserName.Location = new System.Drawing.Point(9, 9);
            this.Login_Txt_UserName.Name = "Login_Txt_UserName";
            this.Login_Txt_UserName.Size = new System.Drawing.Size(231, 22);
            this.Login_Txt_UserName.TabIndex = 1;
            this.Login_Txt_UserName.Text = "UserName";
            this.Login_Txt_UserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Login_Txt_UserName.Enter += new System.EventHandler(this.Login_Txt_UserName_Enter);
            this.Login_Txt_UserName.Leave += new System.EventHandler(this.Login_Txt_UserName_Leave);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Player.Properties.Resources.boat_at_sunrise_in_ha_long_bay_vietnam_wallpaper;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.extendedPanel1);
            this.DoubleBuffered = true;
            this.Name = "Login";
            this.Size = new System.Drawing.Size(1099, 532);
            this.Load += new System.EventHandler(this.Login_Load);
            this.extendedPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ExtendedPanel extendedPanel1;
        private System.Windows.Forms.Button Login_Btn_Exit;
        private System.Windows.Forms.Button Login_Btn_Login;
        private System.Windows.Forms.TextBox Login_Txt_UserName;
        private System.Windows.Forms.TextBox Login_Txt_PassWord;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
