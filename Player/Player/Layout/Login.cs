﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Player.Custom;
using Player.Entity;
using Player.Network;
using Player.Properties;

namespace Player.Layout
{
    public partial class Login : UserControl
    {
        private readonly Action _showMainPage;

        public Login(Action showMainPage)
        {
            _showMainPage = showMainPage;
            InitializeComponent();
        }

        #region Event

        private void Login_Btn_Login_Click(object sender, EventArgs e)
        {
            Login_Btn_Login.Enabled = false;
            if (Login_Txt_UserName.Text == string.Empty || Login_Txt_PassWord.Text == string.Empty)
            {
                Notify.Error(Resources.Not_User_Password);
                return;
            }
            User user = new User
            {
                UserName = Login_Txt_UserName.Text,
                PassWord = Login_Txt_PassWord.Text
            };
            UserLoginReq msg = new UserLoginReq
            {

                User = user,
                RequestName = (int)RequestName.Login

            };
            NetworkMain.Send(msg, LoginResponse);
        }

        #endregion



        #region Response


        public void LoginResponse(string data)
        {
            UserLoginRes userLoginRes = NetworkUtil.ParseJson<UserLoginRes>(data);
            if (userLoginRes.ResourceKey == (int)ResourceKey.Success)
            {
                _showMainPage();
                DefaultValue.MyUser = userLoginRes.User;
                if (FuncGlobal.ChangeTitleFullName != null)
                {
                    FuncGlobal.ChangeTitleFullName();
                }
            }
            else
            {
                Notify.Warning("Dang nhap khong thanh cong");
                Login_Btn_Login.Enabled = true;
            }
        }

        #endregion

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void Login_Txt_UserName_Enter(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals("UserName"))
            {
                textBoxHere.Text = string.Empty;
                textBoxHere.ForeColor = Color.Black;
            }
        }

        private void Login_Txt_UserName_Leave(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals(String.Empty))
            {
                textBoxHere.Text = "UserName";
                textBoxHere.ForeColor = SystemColors.ScrollBar;
            }
        }

        private void Login_Txt_PassWord_Enter(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals("PassWord"))
            {
                textBoxHere.Text = string.Empty;
                textBoxHere.ForeColor = Color.Black;
                textBoxHere.PasswordChar = '*';
            }
        }

        private void Login_Txt_PassWord_Leave(object sender, EventArgs e)
        {
            TextBox textBoxHere = (TextBox)sender;
            if (textBoxHere.Text.Equals(String.Empty))
            {
                textBoxHere.Text = "PassWord";
                textBoxHere.ForeColor = SystemColors.ScrollBar;
                textBoxHere.PasswordChar = '\0';
            }
        }
    }
}
