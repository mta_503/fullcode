﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Player.Layout
{
    public partial class ViewAnswer : Form
    {
        public ViewAnswer(string answer,string explain)
        {
            InitializeComponent();
            AnswerLabel.Text = answer;
            ExplainLabel.Text = explain;
        }
    }
}
