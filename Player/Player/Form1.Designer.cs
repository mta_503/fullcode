﻿using System;
using Player.Layout;

namespace Player
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Login

        public void ShowLoginPage()
        {
            Page_Login = new Layout.Login(ShowLoginExamPage);

            Page_Login.Location = new System.Drawing.Point(0, 0);
            Page_Login.Name = "Page_Login";
            Page_Login.Size = new System.Drawing.Size(1099, 532);
            Page_Login.TabIndex = 0;

            Controls.Add(Page_Login);
        }

        #endregion

        #region ShowMainPage

        public void ShowMainPage()
        {
            mainPage1 = new MainPage();
            // mainPage1
            // 
            mainPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            mainPage1.Location = new System.Drawing.Point(121, 0);
            mainPage1.Name = "mainPage1";
            mainPage1.Size = new System.Drawing.Size(978, 582);
            mainPage1.TabIndex = 0;
            // 

            BeginInvoke((Action)(() =>
            {
                Page_Login.Visible = false;
                MenuLeft.Visible = true;
                Controls.Add(mainPage1);
            }));


        }

        #endregion

        #region ShowLoginExamPage

        public void ShowLoginExamPage()
        {
            LoginExamLayout = new LoginExam(ShowReadyPage);
            // LoginExamLayout
            // 
            LoginExamLayout.Location = new System.Drawing.Point(121, 0);
            LoginExamLayout.Name = "LoginExamLayout";
            LoginExamLayout.Size = new System.Drawing.Size(978, 532);
            LoginExamLayout.TabIndex = 0;
            // 
            BeginInvoke((Action)(() =>
            {
                Page_Login.Visible = false;
                MenuLeft.Visible = true;
                Controls.Add(LoginExamLayout);
            }));

        }

        #endregion

        #region ShowReadyPage

        public void ShowReadyPage()
        {
            readyLayout1 = new ReadyLayout(ShowPlayExam);
            // readyLayout1
            // 
            readyLayout1.Location = new System.Drawing.Point(121, 0);
            readyLayout1.Name = "readyLayout1";
            readyLayout1.Size = new System.Drawing.Size(978, 532);
            readyLayout1.TabIndex = 2;
            // 
            BeginInvoke((Action)(() =>
            {
                LoginExamLayout.Visible = false;
                Controls.Add(readyLayout1);
            }));
        }

        #endregion

        #region ShowPlayExam

        public void ShowPlayExam()
        {
            this.playExam1 = new PlayExam();
            // playExam1
            //
            this.playExam1.BackColor = System.Drawing.Color.White;
            this.playExam1.Location = new System.Drawing.Point(121, 0);
            this.playExam1.Name = "playExam2";
            this.playExam1.Size = new System.Drawing.Size(978, 532);
            this.playExam1.TabIndex = 2;
            // 

            BeginInvoke((Action)(() =>
            {
                readyLayout1.Visible = false;
                this.Controls.Add(this.playExam1);
            }));

        }

        #endregion


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuLeft = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.TitleFullName = new System.Windows.Forms.Label();
            this.MenuLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuLeft
            // 
            this.MenuLeft.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.MenuLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MenuLeft.Controls.Add(this.label1);
            this.MenuLeft.Controls.Add(this.TitleFullName);
            this.MenuLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuLeft.Location = new System.Drawing.Point(0, 0);
            this.MenuLeft.Name = "MenuLeft";
            this.MenuLeft.Size = new System.Drawing.Size(121, 531);
            this.MenuLeft.TabIndex = 1;
            this.MenuLeft.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Teal;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Image = global::Player.Properties.Resources.User_Preppy_Green_icon;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 48);
            this.label1.TabIndex = 3;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TitleFullName
            // 
            this.TitleFullName.BackColor = System.Drawing.Color.Teal;
            this.TitleFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleFullName.ForeColor = System.Drawing.Color.Honeydew;
            this.TitleFullName.Location = new System.Drawing.Point(0, 48);
            this.TitleFullName.Margin = new System.Windows.Forms.Padding(0);
            this.TitleFullName.Name = "TitleFullName";
            this.TitleFullName.Size = new System.Drawing.Size(117, 37);
            this.TitleFullName.TabIndex = 2;
            this.TitleFullName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 531);
            this.Controls.Add(this.MenuLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.MenuLeft.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        #region Init Parameter

        private Layout.Login Page_Login;
        private Layout.MainPage mainPage1;


        #endregion

        private LoginExam LoginExamLayout;
        private System.Windows.Forms.FlowLayoutPanel MenuLeft;
        private ReadyLayout readyLayout1;
        private PlayExam playExam1;
        private System.Windows.Forms.Label TitleFullName;
        private System.Windows.Forms.Label label1;
    }
}

