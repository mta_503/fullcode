﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using Player.Custom;
using Player.Properties;
using Newtonsoft.Json.Linq;

namespace Player.Network
{
    public class NetworkMain
    {

        public static TcpClient Client;
        public static Thread ConnectThread;
        public static Action<string> Callback;
        public static bool Ready;
        public static Stream Stream;
        public static StreamReader StreamReader;
        public static StreamWriter StreamWriter;


        public static void ConnectServer()
        {
            ConnectThread = new Thread(KetNoiDenServerThread) { IsBackground = true };
            ConnectThread.Start();
        }

        public static void KetNoiDenServerThread()
        {
            try
            {

                Client = new TcpClient();
                JObject config = new JObject();
                config = Config.Instance.LoadConfig();
                Client.Connect(config["ip_server"].ToString(), 9999);

                Thread listenThread = new Thread(ClientListen) { IsBackground = true };
                listenThread.Start(Client);
            }
            catch (Exception)
            {
                Notify.Error(Resources.Lost_Connection);
                Application.Exit();
            }

        }

        public static void ClientListen(object obj)
        {
            Stream = Client.GetStream();
            StreamReader = new StreamReader(Stream);
            StreamWriter = new StreamWriter(Stream) { AutoFlush = true };

            try
            {
                Ready = true;
                while (true)
                {
                    string data = StreamReader.ReadLine();

                    if (Callback != null)
                    {
                        try
                        {
                            Action<string> callbackClone = Callback;
                            Callback = null;
                            callbackClone(data);
                        }
                        catch (Exception e)
                        {
                            Notify.Error(e.ToString());
                        }
                    }
                    else
                    {
                        BroadcastMessage broadcastMessage = NetworkUtil.ParseJson<BroadcastMessage>(data);
                        if (broadcastMessage.BroadcastName == (int)BroadcastType.LoginExam)
                        {
                            if (BroadcastFunction.OnResLoginExam != null)
                            {
                                BroadcastFunction.OnResLoginExam(data);
                            }
                        }
                        else if (broadcastMessage.BroadcastName == (int)BroadcastType.BeginExam)
                        {
                            if (BroadcastFunction.OnResBeginExam != null)
                            {
                                BroadcastFunction.OnResBeginExam(data);
                            }
                        }
                        else if (broadcastMessage.BroadcastName == (int)BroadcastType.SendQuestion)
                        {
                            if (BroadcastFunction.OnResQuestion != null)
                            {
                                BroadcastFunction.OnResQuestion(data);
                            }
                        }
                        else if (broadcastMessage.BroadcastName == (int)BroadcastType.SendUserInfo)
                        {
                            if (BroadcastFunction.OnResUserInfo != null)
                            {
                                BroadcastFunction.OnResUserInfo(data);
                            }
                        }
                        else if (broadcastMessage.BroadcastName == (int)BroadcastType.SendTime)
                        {
                            if (BroadcastFunction.OnResTime != null)
                            {
                                BroadcastFunction.OnResTime(data);
                            }
                        }
                        else if (broadcastMessage.BroadcastName == (int)BroadcastType.AnswerKd)
                        {
                            if (BroadcastFunction.OnResAnswer != null)
                            {
                                BroadcastFunction.OnResAnswer(data);
                            }
                        }
                        else if (broadcastMessage.BroadcastName == (int)BroadcastType.CalculPointKd)
                        {
                            if (BroadcastFunction.OnResCalculPointKd != null)
                            {
                                BroadcastFunction.OnResCalculPointKd(data);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static void Send(object msg, Action<string> callback)
        {
            while (!Ready) { }
            if (callback != null)
                Callback = callback;
            Thread thread = new Thread(SendInThread) { IsBackground = true };
            thread.Start(msg);
        }

        public static void SendInThread(object data)
        {
            StreamWriter.WriteLine(JsonConvert.SerializeObject(data));
        }

    }
}
