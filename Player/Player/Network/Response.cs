﻿using System;
using System.Collections.Generic;
using Player.Entity;

namespace Player.Network
{
    public class UserLoginRes
    {
        public int RequestName { get; set; }

        public int ResourceKey { get; set; }

        public User User { get; set; }

    }

    public class UpdateQuestionRes
    {
        public int RequestName { get; set; }

        public int ResourceKey { get; set; }

    }

    public class GetListQuestionRes : IDisposable
    {

        public GetListQuestionRes()
        {
            ListQuestion = new List<Question>();
        }

        public List<Question> ListQuestion { get; set; }
        public int RequestName { get; set; }

        public void Dispose()
        {
            ListQuestion = null;
        }
    }

    public class LoginExamRes
    {
        public int RequestName { get; set; }

        public int ResourceKey { get; set; }

    }

}
