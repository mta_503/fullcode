﻿using System;
using System.Text;
using Newtonsoft.Json;
using Player.Custom;

namespace Player.Network
{
    public class NetworkUtil
    {
        
        public static T ParseJson<T>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception e)
            {
                Notify.Error(e.ToString());
                return default(T);
            }
        }

    }
}
