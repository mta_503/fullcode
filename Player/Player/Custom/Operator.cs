﻿namespace Player.Custom
{
    public class Operator
    {
        public static int GetGradeIdByName(string name)
        {
            if (name.Equals(LangVi.Ten)) return (int)GradeEnum.Ten;
            if (name.Equals(LangVi.Eleven)) return (int)GradeEnum.Eleven;
            if (name.Equals(LangVi.Twelve)) return (int)GradeEnum.Twelve;
            return 0;
        }

        public static int GetLevelIdByName(string name)
        {
            if (name.Equals(LangVi.Easy)) return (int)LevelEnum.Easy;
            if (name.Equals(LangVi.Normal)) return (int)LevelEnum.Normal;
            if (name.Equals(LangVi.Hard)) return (int)LevelEnum.Hard;
            return 0;
        }

        public static int GetQuestTypeIdByName(string name)
        {
            if (name.Equals(LangVi.Choice)) return (int)QuestionType.Choice;
            if (name.Equals(LangVi.NotChoice)) return (int)QuestionType.NotChoice;
            return 0;
        }

        public static string GetGradeName(long? grade)
        {
            if (grade == (int)GradeEnum.Ten) return LangVi.Ten;
            if (grade == (int)GradeEnum.Eleven) return LangVi.Eleven;
            if (grade == (int)GradeEnum.Twelve) return LangVi.Twelve;
            return string.Empty;
        }

        public static string GetLevelName(long? level)
        {
            if (level == (int)LevelEnum.Easy) return LangVi.Easy;
            if (level == (int)LevelEnum.Normal) return LangVi.Normal;
            if (level == (int)LevelEnum.Hard) return LangVi.Hard;
            return string.Empty;
        }

    }
}
