﻿using System.Windows.Forms;

namespace Player.Custom
{
    public class Notify
    {
        public static void Normal(string content)
        {
            MessageBox.Show(content);
        }

        public static void Warning(string content)
        {
            MessageBox.Show(content, null,MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void Error(string content)
        {
            MessageBox.Show(content, null, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
