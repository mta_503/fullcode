﻿using System;
using System.Net.Sockets;

namespace Player.Custom
{
    public class SendThread
    {
        public Object Data { get; set; }

        public Socket Socket { get; set; }
    }
}
