﻿using System.Collections.Generic;
using Player.Entity;

namespace Player.Custom
{
    public class BroadcastMessage
    {
        public object Data { get; set; }

        public int BroadcastName { get; set; }
    }

    public class BroadcastLoginExam
    {
        public List<UserState> ListUserState { get; set; }

        public int BroadcastName { get; set; }
    }

    public class BroadcastExamBegin
    {
        public int StateExam { get; set; }

        public int BroadcastName { get; set; }
    }

    public class BroadcastExamQuestion
    {
        public Question Question { get; set; }

        public string UserName { get; set; }

        public int QuestionIndex { get; set; }

        public int BroadcastName { get; set; }

        public List<Answer> ListAnswer { get; set; }
    }

    public class BroadcastUserInfoExam
    {
        public List<UserInfoExam> ListUserInfoExam { get; set; }

        public int BroadcastName { get; set; }
    }

    public class BroadcastTimeExam
    {
        public int TimeCurrentValue { get; set; }

        public int BroadcastName { get; set; }
    }

    public class BroadcastAnswerKd
    {
        public string Answer { get; set; }

        public string UserName { get; set; }

        public int BroadcastName { get; set; }
    }

    public class BroadcastPointUserKd
    {
        public int Point { get; set; }

        public string UserName { get; set; }

        public int BroadcastName { get; set; }
    }

}
