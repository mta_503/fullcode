﻿using System;

namespace Player.Custom
{
    public class BroadcastFunction
    {
        public static Action<string> OnResLoginExam;

        public static Action<string> OnResBeginExam;

        public static Action<string> OnResQuestion;

        public static Action<string> OnResUserInfo;

        public static Action<string> OnResTime;

        public static Action<string> OnResAnswer;

        public static Action<string> OnResCalculPointKd;
    }
}
