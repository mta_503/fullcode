﻿namespace Player.Custom
{
    public class UserControlName
    {
        public string Point { get; set; }

        public string AnswerView { get; set; }

        public string FullName { get; set; }

        public string RightButton { get; set; }

        public string FailButton { get; set; }
    }
}
