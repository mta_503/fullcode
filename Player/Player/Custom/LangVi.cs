﻿namespace Player.Custom
{
    public class LangVi
    {
        public static string Ten = "10";
        public static string Eleven = "11";
        public static string Twelve = "12";
        public static string Easy = "Dễ";
        public static string Normal = "Trung Bình";
        public static string Hard = "Khó";
        public static string Choice = "Trắc Nghiệm";
        public static string NotChoice = "Tự Luận";
    }
}
