﻿using Player.Entity;

namespace Player.Custom
{
    public class UserState
    {
        public User User { get; set; }
        public int State { get; set; }
    }
}
