﻿using System;
using Client.Layout;

namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Login

        public void ShowLoginPage()
        {
            Page_Login = new Layout.Login(ShowMainPage);

            this.Page_Login.Location = new System.Drawing.Point(200, 100);
            this.Page_Login.Name = "Page_Login";
            this.Page_Login.Size = new System.Drawing.Size(700, 400);
            this.Page_Login.TabIndex = 0;

            this.Controls.Add(this.Page_Login);
        }

        public void ShowMainPage()
        {
            this.mainPage1 = new MainPage();
            // mainPage1
            // 
            this.mainPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPage1.Location = new System.Drawing.Point(0, 0);
            this.mainPage1.Name = "mainPage1";
            this.mainPage1.Size = new System.Drawing.Size(1100, 600);
            this.mainPage1.TabIndex = 0;
            // 

            this.BeginInvoke((Action)(() =>
            {
                this.Page_Login.Visible = false;
                this.Controls.Add(this.mainPage1);
            }));

            
        }

        #endregion

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SuspendLayout();
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1134, 561);
            this.Name = "Form1";
            this.Text = "Phần mềm hỗ trợ học tập EM YÊU ĐẤT MỎ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        #region Init Parameter

        private Layout.Login Page_Login;
        private Layout.MainPage mainPage1;

        #endregion


    }
}

