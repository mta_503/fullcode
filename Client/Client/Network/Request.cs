﻿using System.Collections.Generic;
using Server.Database;

namespace Client.Network
{
    public class UserLoginReq
    {
        
        public User User { get; set; }

        public int RequestName { get; set; }

    }

    public class UpdateQuestionReq
    {
        public Question Question { get; set; }

        public List<Answer> Answers { get; set; }

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

    public class GetListQuestionReq
    {
        public int ContestType { get; set; }

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

    public class GetListPackageReq
    {

        public int RequestType { get; set; }

        public int RequestName { get; set; }
    }

    public class UpdatePackageReq
    {

        public Package Package { get; set; }

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

}
