﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Client.Custom;
using Client.Properties;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Client.Network
{
    public class NetworkMain
    {

        public static TcpClient Client;
        public static Thread ConnectThread;
        public static Action<string> Callback;
        public static bool Ready;
        public static Stream Stream;
        public static StreamReader StreamReader;
        public static StreamWriter StreamWriter;


        public static void ConnectServer()
        {
            ConnectThread = new Thread(KetNoiDenServerThread) {IsBackground = true};
            ConnectThread.Start();
        }

        public static void KetNoiDenServerThread()
        {
            try
            {

                Client = new TcpClient();
                JObject config = new JObject();
                config = Config.Instance.LoadConfig();
                Client.Connect(config["ip_server"].ToString(),9999);

                Thread listenThread = new Thread(ClientListen) { IsBackground = true };
                listenThread.Start(Client);
            }
            catch (Exception ex)
            {
                Notify.Error(Resources.Lost_Connection);
               // MessageBox.Show(ex.ToString());
                Application.Exit();
            }

        }

        public static void ClientListen(object obj)
        {
            Stream = Client.GetStream();
            StreamReader = new StreamReader(Stream);
            StreamWriter = new StreamWriter(Stream) { AutoFlush = true };
            int count = 0;
            try
            {
                Ready = true;
                while (true)
                {
                    string data = StreamReader.ReadLine();
                    
                    if (Callback != null)
                    {
                        try
                        {
                            if (Callback.Method.Name == "OnResponseGetListQuestion") { count++; }

                            if(count == 3)
                            {
                                count = 0;
                            }
                            else
                            {
                                Action<string> callbackClone = Callback;
                                Callback = null;
                                callbackClone(data);
                            }


                        }
                        catch (Exception e)
                        {
                            Notify.Error(e.ToString());
                        }
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(Resources.Lost_Connection);
            }
        }

        public static void Send(object msg, Action<string> callback)
        {
            while (!Ready) { }
            Callback = callback;
            Thread thread = new Thread(SendInThread) { IsBackground = true };
            thread.Start(msg);
        }

        public static void SendInThread(object data)
        {
            StreamWriter.WriteLine(JsonConvert.SerializeObject(data));
        }

    }
}
