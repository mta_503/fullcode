﻿using System.Text;
using Client.Custom;
using Newtonsoft.Json;
using System;

namespace Client.Network
{
    public class NetworkUtil
    {

        public static T ParseJson<T>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception e)
            {
                Notify.Error(e.ToString());
                return default(T);
            }
        }

    }
}
