﻿namespace Client.Custom
{
    public enum RequestName
    {
        Login = 1,
        UpdateQuestion = 2,
        GetListQuestion = 3,
        GetListPackage = 9,
        UpdatePackage = 10
    }

    public enum RequestType
    {
        GetAll = 1,
        Insert = 2,
        Update = 3,
        GetByContestType = 4,
        Delete = 5
    }

    public enum ResourceKey
    {
        Success = 1,
        Error = 2,
        FailLogin = 3
    }

    public enum GradeEnum
    {
        Ten = 10,
        Eleven = 11,
        Twelve = 12
    }

    public enum LevelEnum
    {
        Easy = 1,
        Normal = 2,
        Hard = 3
    }

    public enum QuestionType
    {
        Choice = 1,
        NotChoice = 2
    }

    public enum QuestionStatus
    {
        Active = 1,
        InActive = 2
    }

    public enum ContestType
    {
        Kd = 1,
        Vcnv = 2,
        Tt = 3,
        Vd = 4
    }

    public enum PacketStatus
    {
      Active = 1,
      InActive = 2   
    }

}
