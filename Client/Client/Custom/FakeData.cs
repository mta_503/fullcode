﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.Database;

namespace Client.Custom
{
 public   class FakeData
    {
        public static List<User> LoadUsers()
        {
            User user1 = new User();
            user1.UserName = "Nguyen Duc Huy";
            user1.FullName = "Nguyễn Đức Huy";
            user1.Email = "nguyenduchuy@gmail.com";
            user1.BirthDay = "01/01/2000";
            user1.Phone = "0987654321";
            user1.Sex = "Nam";
            user1.NameClass = "Lớp 11A1";


            User user2 = new User();
            user2.UserName = "Nguyen Huy Hoang";
            user2.FullName = "Nguyễn Huy Hoàng";
            user2.Email = "nguyenhuyhoang@gmail.com";
            user2.BirthDay = "01/12/2000";
            user2.Phone = "0987654321";
            user2.Sex = "Nam";
            user2.NameClass = "Lớp 11A2";

            User user3 = new User();
            user3.UserName = "Nguyen Huy A";
            user3.FullName = "Nguyễn Huy A";
            user3.Email = "nguyenhuya@gmail.com";
            user3.BirthDay = "01/12/2000";
            user3.Phone = "0987654321";
            user3.Sex = "Nam";
            user3.NameClass = "Lớp 11A3";

            User user4 = new User();
            user4.UserName = "Nguyen Huy B";
            user4.FullName = "Nguyễn Huy B";
            user4.Email = "nguyenhuyb@gmail.com";
            user4.BirthDay = "01/12/2000";
            user4.Phone = "0987654321";
            user4.Sex = "Nam";
            user4.NameClass = "Lớp 11A4";

            User user5 = new User();
            user5.UserName = "Nguyen Huy C";
            user5.FullName = "Nguyễn Huy C";
            user5.Email = "nguyenhuycg@gmail.com";
            user5.BirthDay = "01/12/2000";
            user5.Phone = "0987654321";
            user5.Sex = "Nam";
            user5.NameClass = "Lớp 11A5";



            List<User> listUser = new List<User>();
            listUser.Add(user1);
            listUser.Add(user2);
            listUser.Add(user3);
            listUser.Add(user4);
            listUser.Add(user5);

            return listUser;

        }

        public static List<Question> GetQuestions()
        {
            Question question1 = new Question();
            question1.QuesCont = "El Nino  nói về hiện tượng gì ?";
            question1.Answer = "Thời tiết";
            question1.Grade = 10;
            question1.QuesType = 1;
            question1.Cat = DefaultValue.TopicList[0];
            question1.ContestType = 2;
            question1.QuesStatus = 1;
            question1.Point = 10;
            question1.Level = 1;

            Question question2 = new Question();
            question2.QuesCont = "Lãnh tụ fidel casto là người nước nào  ?";
            question2.Answer = "cuba";
            question2.Grade = 10;
            question2.QuesType = 1;
            question2.Cat = DefaultValue.TopicList[2];
            question2.ContestType = 2;
            question2.QuesStatus = 1;
            question2.Point = 10;
            question2.Level = 1;

            Question question3 = new Question();
            question3.QuesCont = "Nước nào có diện tích nhỏ nhất thế giới ?";
            question3.Answer = "Vatican";
            question3.Grade = 11;
            question3.QuesType = 1;
            question3.Cat = DefaultValue.TopicList[3];
            question3.ContestType = 2;
            question3.QuesStatus = 1;
            question3.Point = 10;
            question3.Level = 1;

            Question question4 = new Question();
            question4.QuesCont = "Động cơ hơi nước đầu tiên được chế tạo vào năm nào  ?";
            question4.Answer = "1769";
            question4.Grade = 20;
            question4.QuesType = 1;
            question1.Cat = DefaultValue.TopicList[0];
            question4.ContestType = 2;
            question4.QuesStatus = 1;
            question4.Point = 10;
            question4.Level = 3;


            Question question5 = new Question();
            question5.QuesCont = "Khí gì gây gây cười  ?";
            question5.Answer = "NO2";
            question5.Grade = 10;
            question5.QuesType = 1;
            question5.Cat = DefaultValue.TopicList[0];
            question5.ContestType = 2;
            question5.QuesStatus = 1;
           question5.Point = 10;
            question5.Level = 1;

            List<Question> listqQuestions = new List<Question>();
            listqQuestions.Add(question1);
            listqQuestions.Add(question2);
            listqQuestions.Add(question3);
            listqQuestions.Add(question4);
            listqQuestions.Add(question5);

            return listqQuestions;



        }
    }
}
