﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Client.Custom
{
    public class DefaultValue
    {
        public static List<String> TopicList = new List<string> { "Toán Học", "Văn Học", "Tiếng Anh", "Hóa Học", "Vật Lý", "Sử Học", "Địa Lý" };
        public  static  List<String> ErrorList =

        new List<string>
        {
            "Nội dung câu hỏi không được để trống",
            "Câu trả lời không được để trống",
            "Điểm là số và lớn hơn 0",
            "Bạn phải chọn chủ để cho câu hỏi",
            "Câu hỏi loại trắc nhiệm phải có đáp án",
            "Tên gói câu hỏi không được để trống "
        };
    }
}
