﻿using System;
using System.Net.Sockets;

namespace Client.Custom
{
    public class SendThread
    {
        public Object Data { get; set; }

        public Socket Socket { get; set; }
    }
}
