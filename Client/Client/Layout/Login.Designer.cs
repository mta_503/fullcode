﻿namespace Client.Layout
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.Login_Txt_PassWord = new System.Windows.Forms.TextBox();
            this.Login_Txt_UserName = new System.Windows.Forms.TextBox();
            this.Login_Btn_Login = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Login_Btn_Exit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Login_Txt_PassWord
            // 
            this.Login_Txt_PassWord.Location = new System.Drawing.Point(251, 254);
            this.Login_Txt_PassWord.Multiline = true;
            this.Login_Txt_PassWord.Name = "Login_Txt_PassWord";
            this.Login_Txt_PassWord.Size = new System.Drawing.Size(200, 30);
            this.Login_Txt_PassWord.TabIndex = 0;
            // 
            // Login_Txt_UserName
            // 
            this.Login_Txt_UserName.Location = new System.Drawing.Point(251, 185);
            this.Login_Txt_UserName.Multiline = true;
            this.Login_Txt_UserName.Name = "Login_Txt_UserName";
            this.Login_Txt_UserName.Size = new System.Drawing.Size(200, 30);
            this.Login_Txt_UserName.TabIndex = 0;
            // 
            // Login_Btn_Login
            // 
            this.Login_Btn_Login.BackColor = System.Drawing.Color.White;
            this.Login_Btn_Login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Login_Btn_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Btn_Login.ForeColor = System.Drawing.Color.Black;
            this.Login_Btn_Login.Location = new System.Drawing.Point(146, 330);
            this.Login_Btn_Login.Name = "Login_Btn_Login";
            this.Login_Btn_Login.Size = new System.Drawing.Size(160, 40);
            this.Login_Btn_Login.TabIndex = 1;
            this.Login_Btn_Login.Text = "Đăng Nhập";
            this.Login_Btn_Login.UseVisualStyleBackColor = false;
            this.Login_Btn_Login.Click += new System.EventHandler(this.Login_Btn_Login_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(101, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "User name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(105, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // Login_Btn_Exit
            // 
            this.Login_Btn_Exit.BackColor = System.Drawing.Color.White;
            this.Login_Btn_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_Btn_Exit.ForeColor = System.Drawing.Color.Black;
            this.Login_Btn_Exit.Location = new System.Drawing.Point(380, 330);
            this.Login_Btn_Exit.Name = "Login_Btn_Exit";
            this.Login_Btn_Exit.Size = new System.Drawing.Size(160, 40);
            this.Login_Btn_Exit.TabIndex = 1;
            this.Login_Btn_Exit.Text = "Thoát";
            this.Login_Btn_Exit.UseVisualStyleBackColor = false;
            this.Login_Btn_Exit.Click += new System.EventHandler(this.Login_Btn_Exit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(84, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(251, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(418, 129);
            this.label3.TabIndex = 6;
            this.label3.Text = "SỞ GD && ĐT QUẢNG NINH\r\n\r\nTRƯỜNG TRUNG HỌC PHỔ THÔNG BẠCH ĐẰNG\r\n\r\nPHẦN MỀM HỖ TRỢ" +
    " HỌC TẬP EM YÊU ĐẤT MỎ\r\n\r\n";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Login_Btn_Exit);
            this.Controls.Add(this.Login_Btn_Login);
            this.Controls.Add(this.Login_Txt_UserName);
            this.Controls.Add(this.Login_Txt_PassWord);
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Name = "Login";
            this.Size = new System.Drawing.Size(700, 400);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Login_Txt_PassWord;
        private System.Windows.Forms.TextBox Login_Txt_UserName;
        private System.Windows.Forms.Button Login_Btn_Login;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Login_Btn_Exit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
    }
}
