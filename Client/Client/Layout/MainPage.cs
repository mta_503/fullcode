﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Client.Custom;
using Client.Network;
using Server.Database;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace Client.Layout
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            Init_QuestionKd();
            //Init_PackageKd();
            BindUsers();
            BindQuestions();
        }
        /* Binding list Users */
        private void BindUsers()
        {
            var listUser = new BindingList<User>(FakeData.LoadUsers());
            dataGridView1.DataSource = listUser;
            dataGridView1.Columns[0].HeaderText = "User name";
            dataGridView1.Columns[1].HeaderText = "Mật khẩu";
            dataGridView1.Columns[2].HeaderText = "Email";
            dataGridView1.Columns[3].HeaderText = "Số điện thoại";
            dataGridView1.Columns[4].HeaderText = "Lớp";
            dataGridView1.Columns[5].HeaderText = "Họ tên";
            dataGridView1.Columns[6].HeaderText = "Giới tính";
            dataGridView1.Columns[7].HeaderText = "Ngày sinh";

            dataGridView1.Columns[1].Visible = false;


        }

        /*  end binding list users */

        private void BindQuestions()
        {

            //var listQuestions = new BindingList<Question>(FakeData.GetQuestions());
            //Dgv_ListQuestion_KD.DataSource = listQuestions;


        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void Ques_KD_Add_BtnAdd_Click(object sender, EventArgs e)
        {
            bool  isValid = true;
            UpdateQuestionReq updateQuestionReq = new UpdateQuestionReq();
            Question newQuestion = new Question();
            if (!string.IsNullOrEmpty(Ques_KD_Add_TxtQuesCont.Text))
            {
        
                newQuestion.QuesCont = Ques_KD_Add_TxtQuesCont.Text;
            }
            else
            {
                label5.Text = DefaultValue.ErrorList[0];
                isValid = false;

            }

            if (!string.IsNullOrEmpty(Ques_KD_Add_Answer.Text))
            {

                newQuestion.Answer = Ques_KD_Add_Answer.Text;
            }
            else
            {
                label5.Text = DefaultValue.ErrorList[1];
                isValid = false;
            }

           
            var grade = Operator.GetGradeIdByName(Ques_KD_Add_Grade.Text);
            if (grade > 0)
            {
                newQuestion.Grade = grade;
            }
            else
            {
                Notify.Error("Grade Not Exist");
                return;
            }
            var level = Operator.GetLevelIdByName(Ques_KD_Add_Level.Text);
            if (level > 0)
            {
                newQuestion.Level = level;
            }
            else
            {
                newQuestion.Level = 1;
               
            }
            var quesType = Operator.GetQuestTypeIdByName(Ques_KD_Add_QuesType.Text);
            if (quesType > 0)
            {
                newQuestion.QuesType = quesType;
            }
            else
            {
                Notify.Error("QuestionType Not Exist");
                return;
            }
            newQuestion.Explain = Ques_KD_Add_Explain.Text;
            long point = 0;
            bool isNumeric = long.TryParse(Ques_KD_Add_Point.Text, out point);
            if (isNumeric)
            {
                if (point > 0)
                {
                    newQuestion.Point = point;
                   
                }
                else
                {
                    isValid = false;
                    label5.Text = DefaultValue.ErrorList[2];
                    return;
                }
      

            }
            else
            {
                label5.Text = DefaultValue.ErrorList[2];
                isValid = false;
            }
            if (!string.IsNullOrEmpty(Ques_KD_Add_Topic.Text))
            {
                newQuestion.Cat = Ques_KD_Add_Topic.Text;
            }
            else
            {
                label5.Text = DefaultValue.ErrorList[3];
                isValid = false;
            }
           
            newQuestion.QuesStatus = (long)QuestionStatus.Active;
            newQuestion.ContestType = (long) ContestType.Kd;



            updateQuestionReq.Question = newQuestion;
            updateQuestionReq.RequestName = (int) RequestName.UpdateQuestion;
            updateQuestionReq.RequestType = (int) RequestType.Insert;

            if (newQuestion.QuesType == (int)QuestionType.Choice)
            {
                List<Answer> answers = new List<Answer>();

                foreach (DataGridViewRow row in Ques_KD_Add_ListAnswer.Rows)
                {
                    if (row.Cells["AnsName"].Value == null)
                    {
                        
                        continue;
                    }
                    answers.Add(new Answer
                    {
                        AnsName = row.Cells["AnsName"].Value.ToString(),
                        AnsCont = row.Cells["AnsCont"].Value.ToString()
                    });
                }

                updateQuestionReq.Answers = answers;
                if (answers.Count == 0)
                {
                    label5.Text = DefaultValue.ErrorList[4];
                    isValid = false;
                }
            }

            if (isValid)
            {
               
                NetworkMain.Send(updateQuestionReq, OnresUpdateQuestion);

            }
            return;


        }

        private void OnresUpdateQuestion(string data)
        {
            Init_QuestionKd();
            UpdateQuestionRes updateQuestionRes = NetworkUtil.ParseJson<UpdateQuestionRes>(data);
            Notify.Normal("Thêm hỏi thành công !");
            Ques_KD_Add_TxtQuesCont.Text = "";
            Ques_KD_Add_Answer.Text = "";
            Ques_KD_Add_ListAnswer.Refresh();
            label5.Text = "";
            return;


        }

        private void tabPage10_Click(object sender, EventArgs e)
        {
            
        }

        private void tabControl4_Selected(object sender, TabControlEventArgs e)
        {
            foreach (var topic in DefaultValue.TopicList)
            {
                Ques_KD_Add_Topic.Items.Add(topic);
            }
            Ques_KD_Add_Topic.SelectedIndex = 0;
            Ques_KD_Add_Topic.Sorted = true;
        }

        private void tabControl3_Selected(object sender, TabControlEventArgs e)
        {
            
        }

        private void OnResponseGetListQuestion(string data)
        {
            
            GetListQuestionRes getListQuestionRes = NetworkUtil.ParseJson<GetListQuestionRes>(data);
            var listQues = new BindingList<Question>(getListQuestionRes.ListQuestion);
            Dgv_ListQuestion_KD.DataSource = null;
            Dgv_ListQuestion_KD.DataSource = listQues;
            Dgv_ListQuestion_KD.Columns[0].HeaderText = "Mã câu hỏi";
            Dgv_ListQuestion_KD.Columns[1].HeaderText = "Nội dung câu hỏi";
            Dgv_ListQuestion_KD.Columns[2].HeaderText = "Khối lớp";
            Dgv_ListQuestion_KD.Columns[3].HeaderText = "Độ khó";
            Dgv_ListQuestion_KD.Columns[5].HeaderText = "Chủ đề";
            Dgv_ListQuestion_KD.Columns[6].HeaderText = "Giải thích";
            Dgv_ListQuestion_KD.Columns[7].HeaderText = "Điểm";
            Dgv_ListQuestion_KD.Columns[9].HeaderText = "Trả lời";

            Dgv_ListQuestion_KD.Columns[4].Visible = false;
           // Dgv_ListQuestion_KD.Columns[6].Visible = false;
           // Dgv_ListQuestion_KD.Columns[7].Visible = false;
            //Dgv_ListQuestion_KD.Columns[8].Visible = false;
            Dgv_ListQuestion_KD.Columns[10].Visible = false;


            return;


        }

        private void tabControl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        public void Init_QuestionKd()
        {
            GetListQuestionReq getListQuestionReq = new GetListQuestionReq();
            getListQuestionReq.RequestName = (int)RequestName.GetListQuestion;
            getListQuestionReq.ContestType = (int)ContestType.Kd;
            getListQuestionReq.RequestType = (int)RequestType.GetByContestType;

            NetworkMain.Send(getListQuestionReq, OnResponseGetListQuestion);
            return;
        }

        public void Init_PackageKd()
        {
            GetListPackageReq getListPackageReq = new GetListPackageReq();
            getListPackageReq.RequestName = (int)RequestName.GetListPackage;
            getListPackageReq.RequestType = (int)RequestType.GetAll;

            NetworkMain.Send(getListPackageReq, OnResponseGetListPacket);


        }

        private void OnResponseGetListPacket(string data)
        {
            GetListPackageRes getListPackageRes = NetworkUtil.ParseJson<GetListPackageRes>(data);

            var listPackage = new BindingList<Package>(getListPackageRes.ListPackage);
            dataGridView4.DataSource = listPackage;

            dataGridView4.Columns[0].HeaderText = "Mã gói";
            dataGridView4.Columns[1].HeaderText = "Tên gói";
            dataGridView4.Columns[2].HeaderText = "Trạng thái";
            dataGridView4.Columns[3].HeaderText = "Mã câu hỏi";


        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Ques_KD_Add_QuesType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Ques_KD_Add_QuesType.Text.ToString().Equals("Tự Luận"))
            {
                Ques_KD_Add_ListAnswer.Visible = false;
            }
            else
            {
                Ques_KD_Add_ListAnswer.Visible = true;
            }
        }
        int quesId = 0;
        int questionType = 1;
        private void Dgv_ListQuestion_KD_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                DataGridViewRow row = Dgv_ListQuestion_KD.Rows[e.RowIndex];
                textBox18.Text = row.Cells[1].Value.ToString();
                textBox17.Text = row.Cells[9].Value.ToString();
                textBox16.Text = row.Cells[7].Value.ToString();
                textBox19.Text = row.Cells[6].Value.ToString();
                comboBox3.Text = row.Cells[5].Value.ToString();
                comboBox4.Text = row.Cells[2].Value.ToString();
                comboBox5.Text = row.Cells[3].Value.ToString();
                questionType = int.Parse(row.Cells[8].Value.ToString());
                quesId = int.Parse (row.Cells[0].Value.ToString());
            }


     


        }

        private void tabControl4_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (var topic in DefaultValue.TopicList)
            {
                comboBox3.Items.Add(topic);
            }
           

        }

        private void button6_Click(object sender, EventArgs e)
        {
            bool isValid = true;
            UpdateQuestionReq updateQuestionReq = new UpdateQuestionReq();
            Question updateQuestion = new Question();
            updateQuestion.QuesId = quesId;
            if (!string.IsNullOrEmpty(textBox18.Text))
            {

                updateQuestion.QuesCont = textBox18.Text;
            }
            else
            {
                label60.Text = DefaultValue.ErrorList[0];
                isValid = false;

            }

            if (!string.IsNullOrEmpty(textBox17.Text))
            {

                updateQuestion.Answer = textBox17.Text;
            }
            else
            {
                label60.Text = DefaultValue.ErrorList[1];
                isValid = false;
            }


            int grade = Int32.Parse(comboBox4.Text);
     
                updateQuestion.Grade = grade;

            int level = Int32.Parse(comboBox5.Text);

                updateQuestion.Level = level;
   
            updateQuestion.Explain = textBox19.Text;
            long point = 0;
            bool isNumeric = long.TryParse(textBox16.Text, out point);
            if (isNumeric)
            {
                if (point > 0)
                {

                    updateQuestion.Point = point;
                }
                else
                {
                    updateQuestion.Point = 10;
                }


            }
            else
            {
                label60.Text = DefaultValue.ErrorList[2];
                isValid = false;
            }
            if (!string.IsNullOrEmpty(comboBox3.Text))
            {
                updateQuestion.Cat = comboBox3.Text.ToString();
            }
            else
            {
                label60.Text = DefaultValue.ErrorList[3];
                isValid = false;
            }

            updateQuestion.QuesType = questionType;

            updateQuestion.QuesStatus = (long)QuestionStatus.Active;
            updateQuestion.ContestType = (long)ContestType.Kd;



            updateQuestionReq.Question = updateQuestion;
            updateQuestionReq.RequestName = (int)RequestName.UpdateQuestion;
            updateQuestionReq.RequestType = (int)RequestType.Update;


            if (isValid)
            {
                NetworkMain.Send(updateQuestionReq, OnresUpdate2Question);

            }
            return;
        }

        private void OnresUpdate2Question(string data)
        {
            Init_QuestionKd();
           // UpdateQuestionRes updateQuestionRes = NetworkUtil.ParseJson<UpdateQuestionRes>(data);
            Notify.Normal("Cập nhật hỏi thành công !");

        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            bool isValid = true;
            UpdatePackageReq updatePackageReq = new UpdatePackageReq();
            Package newPackage = new Package();
            if (!string.IsNullOrEmpty(textBox26.Text))
            {

                newPackage.PackName = textBox26.Text;
            }
            else
            {
                label36.Text = DefaultValue.ErrorList[6];
                isValid = false;

            }


            updatePackageReq.Package = newPackage;
            updatePackageReq.RequestName = (int)RequestName.UpdatePackage;
            updatePackageReq.RequestType = (int)RequestType.Insert;

            if (isValid)
            {
                NetworkMain.Send(updatePackageReq, OnResponseUpdatePackage);

            }
            return;
        }

        private void OnResponseUpdatePackage(string data)
        {
            UpdatePackageRes updatePackageRes = NetworkUtil.ParseJson<UpdatePackageRes>(data);
            Notify.Normal("Thêm gói câu hỏi thành công !");
            textBox26.Text = "";

            dataGridView4.Refresh();
            label36.Text = "";
        }

        private void button5_Click(object sender, EventArgs e)
        {
           
            UpdateQuestionReq updateQuestionReq = new UpdateQuestionReq();
            Question updateQuestion = new Question();
            updateQuestion.QuesId = quesId;
          
         


            updateQuestionReq.Question = updateQuestion;
            updateQuestionReq.RequestName = (int)RequestName.UpdateQuestion;
            updateQuestionReq.RequestType = (int)RequestType.Delete;


        
                NetworkMain.Send(updateQuestionReq, OnresUpdate3Question);
            return;
        }
        private void OnresUpdate3Question(string data)
        {
            Init_QuestionKd();
            UpdateQuestionRes updateQuestionRes = NetworkUtil.ParseJson<UpdateQuestionRes>(data);
            Notify.Normal("Xóa hỏi thành công !");

        }
    }
}
