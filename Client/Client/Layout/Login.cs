﻿using System;
using System.Windows.Forms;
using Client.Custom;
using Client.Network;
using Client.Properties;
using Server.Database;

namespace Client.Layout
{
    public partial class Login : UserControl
    {
        private readonly Action _showMainPage;

        public Login(Action showMainPage)
        {
            _showMainPage = showMainPage;
            InitializeComponent();
        }


        #region Event

        private void Login_Btn_Login_Click(object sender, EventArgs e)
        {
            Login_Btn_Login.Enabled = false;
            if (Login_Txt_UserName.Text == string.Empty || Login_Txt_PassWord.Text == string.Empty)
            {
                Notify.Error(Resources.Not_User_Password);
                return;
            }
            User user = new User
            {
                UserName = Login_Txt_UserName.Text,
                PassWord = Login_Txt_PassWord.Text
            };
            UserLoginReq msg = new UserLoginReq
            {
                
                User = user,
                RequestName = (int)RequestName.Login

            };
            NetworkMain.Send(msg, LoginResponse);
        }



        #endregion



        #region Response


        public void LoginResponse(string data)
        {
            UserLoginRes userLoginRes = NetworkUtil.ParseJson<UserLoginRes>(data);
            if (userLoginRes.ResourceKey == (int) ResourceKey.Success)
            {
                _showMainPage();
            }
            else
            {
                Login_Btn_Login.Enabled = true;
                Notify.Warning("Dang nhap khong thanh cong");
            }
        }






        #endregion

        private void Login_Btn_Exit_Click(object sender, EventArgs e)
        {


            Application.Exit();
        }
    }
}
