﻿using System;
using System.Windows.Forms;
using Client.Network;

namespace Client
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            ShowLoginPage();
            NetworkMain.ConnectServer();
        }
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams handleParam = base.CreateParams;
                handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
                return handleParam;
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
        
    }
}
