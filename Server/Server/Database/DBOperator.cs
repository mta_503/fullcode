﻿using System;
using System.Collections.Generic;
using System.Linq;
using Server.Custom;

namespace Server.Database
{
    public class DbOperator
    {
        private static readonly dbEntities OlympiaDb = new dbEntities();

        #region Select Custom

        public static User Login(User user)
        {
            Log.Normal("Check Login");
            User check =
                OlympiaDb.Users
                    .FirstOrDefault(p => p.UserName.Equals(user.UserName) && p.PassWord.Equals(user.PassWord));
            return check;
        }

        public static User GetUserByUserName(string userName)
        {
            return OlympiaDb.Users.FirstOrDefault(p => p.UserName.Equals(userName));
        }

        public static List<Question> GetListQuestion()
        {
            return OlympiaDb.Questions.OrderBy(p => Guid.NewGuid()).Take(12).ToList();
        }

        public static List<Package> GetListPackage()
        {
            return OlympiaDb.Packages.OrderBy(p => Guid.NewGuid()).Take(3).ToList();
        }

        public static List<Answer> GetListAnswerByQuestionId(long questionId)
        {
            return OlympiaDb.Answers.Where(p => p.QuesId == questionId).ToList();
        }

        #endregion


        #region Insert
        public static long InsertPackage(Package package)
        {
            try
            {
                if (!CheckPackage(package)) return 0;
                OlympiaDb.Packages.Add(package);
                OlympiaDb.SaveChanges();
                Log.Normal("Add new question to Database");
                return package.PackId;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return 0;
            }
        }

        public static long InsertQuestion(Question question)
        {
            try
            {
                if (!CheckQuestion(question)) return 0;
                OlympiaDb.Questions.Add(question);
                OlympiaDb.SaveChanges();
                Log.Normal("Add new question to Database");
                return question.QuesId;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return 0;
            }
        }

        public static bool UpdateQuestion(Question question)
        {
            try
            {
                if (!CheckQuestion(question)) { return false; };
                var oldQuestion = OlympiaDb.Questions.FirstOrDefault(p => p.QuesId == question.QuesId);
                if (oldQuestion == null) return false;

                oldQuestion.QuesId = question.QuesId;
                oldQuestion.Cat = question.Cat;
                oldQuestion.Explain = question.Explain;
                oldQuestion.Grade = question.Grade;
                oldQuestion.Level = question.Level;
                oldQuestion.Point = question.Point;
                oldQuestion.QuesCont = question.QuesCont;
                oldQuestion.QuesStatus = question.QuesStatus;
                oldQuestion.QuesType = question.QuesType;

                OlympiaDb.SaveChanges();
                Log.Normal("Update question to Database");
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return false;
            }
        }

        public static bool UpdatePackage(Package package)
        {
            try
            {
                if (!CheckPackage(package)) return false;
                var oldPackage = OlympiaDb.Packages.FirstOrDefault(p => p.PackName == package.PackName);
                if (oldPackage == null) return false;

                oldPackage.PackId = package.PackId;
                oldPackage.PackName = package.PackName;
                oldPackage.PackStatus = package.PackStatus;
                oldPackage.QuestionIds = package.QuestionIds;


                OlympiaDb.SaveChanges();
                Log.Normal("Update package to Database");
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return false;
            }
        }


        public static bool InsertAnswer(List<Answer> answers)
        {
            try
            {
                foreach (var answer in answers)
                {
                    if (OlympiaDb.Questions.FirstOrDefault(p => p.QuesId == answer.QuesId) == null)
                    {
                        Log.Error("Question" + answer.QuesId + "Not Exist");
                        return false;
                    }
                }
                OlympiaDb.Answers.AddRange(answers);
                OlympiaDb.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return false;
            }
        }

        public static List<Question> GetListQuestionByContestType(int contestType)
        {
            return OlympiaDb.Questions.Where(p => p.ContestType == contestType).ToList();
        } 

    #endregion

        #region CheckValidate

        public static bool CheckQuestion(Question question)
        {
            return true;
        }

        public static bool CheckPackage(Package question)
        {
            return true;
        }

        #endregion


        #region Delete

        public static bool DeleteQuestion(long questionId)
        {
            try
            {
               
                var oldPackage = OlympiaDb.Questions.FirstOrDefault(p => p.QuesId == questionId);
                if (oldPackage == null) return false;
                if(oldPackage.QuesType ==1)
                {
                    OlympiaDb.Answers.RemoveRange(OlympiaDb.Answers.Where(x => x.QuesId == questionId));
                    OlympiaDb.SaveChanges();
                }
                OlympiaDb.Questions.Remove(oldPackage);

                OlympiaDb.SaveChanges();
                Log.Normal("Delete question to Database");
                return true;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return false;
            }
        }

        #endregion



    }
}
