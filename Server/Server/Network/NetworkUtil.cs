﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Server.Custom;

namespace Server.Network
{
    public class NetworkUtil
    {
        public static void BroadCast(object res)
        {
            var dataJson = JsonConvert.SerializeObject(res);
            foreach (var client in NetworkMain.Clients)
            {
                byte[] data = Encoding.UTF8.GetBytes(dataJson);
                client.Send(data, SocketFlags.None);
            }
        }

        public static T ParseJson<T>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return default(T);
            }
        }

        public static void Send(Socket socket, string data)
        {
            var stream = new NetworkStream(socket);
            var writer = new StreamWriter(stream) { AutoFlush = true };
            writer.WriteLine(data);
        }

    }
}
