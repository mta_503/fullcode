﻿using Server.Custom;
using Server.Processor;

namespace Server.Network
{
    public class ProcessNav
    {
        public static void Navigate(object messageThread)
        {
            SendThread sendThread = (SendThread) messageThread;

            SendThread sendThreadClone = sendThread.Clone() as SendThread;

            int type = 0;

            if (sendThreadClone != null)
            {
                Message formatData = NetworkUtil.ParseJson<Message>(sendThreadClone.Data);
                type = formatData.RequestName;
            }

            if (type == (int) RequestName.Login)
            {
                Log.Normal("Get message LoginRequest");
                var processLogin = new ProcessLogin(sendThread);
                processLogin.Send();
            }
            else if (type == (int) RequestName.UpdateQuestion)
            {
                Log.Normal("Get message UpdateQuestion");
                var processUpdateQuestion = new ProcessUpdateQuestion(sendThread);
                processUpdateQuestion.Send();
            }
            else if (type == (int)RequestName.GetListQuestion)
            {
                Log.Normal(("Get message GetListQuestions"));
                var proccessGetQuestions = new ProcessGetListQuestion(sendThread);
                proccessGetQuestions.Send();
            }
//            else if (type == (int)RequestName.UpdatePackage)
//            {
//                Log.Normal("Get message UpdatePackage");
//                var processUpdatePackage = new ProcessUpdatePackage(sendThread);
//                processUpdatePackage.Send();
//            }
//            else if (type == (int)RequestName.GetListPackage)
//            {
//                Log.Normal(("Get message GetListPackage"));
//                var processGetListPackage = new ProcessGetListPackage(sendThread);
//                processGetListPackage.Send();
//            }
            else if (type == (int)RequestName.LoginExam)
            {
                Log.Normal("Login exam");
                var processLoginExam = new ProcessLoginExam(sendThread);
                processLoginExam.Send();
            }
            else if (type == (int)RequestName.StateChangeExam)
            {
                Log.Normal("State change exam request");
                var processStateChangeExam = new ProcessStateChangeExam(sendThread);
                processStateChangeExam.Send();
            }
            else if (type == (int)RequestName.AnswerKd)
            {
                Log.Normal("Send Answer");
                var processAnswerKd = new ProcessAnswerKd(sendThread);
                processAnswerKd.Send();
            }
            else if (type == (int)RequestName.CalculatePointKd)
            {
                Log.Normal("Calculator Point User Khoi dong");
                var processCalculPointKd = new ProcessCalculPointKd(sendThread);
                processCalculPointKd.Send();
            }
            else if (type == (int)RequestName.NextKd)
            {
                Log.Normal("Chuyen cau hoi tiep theo");
                var processNextKd = new ProcessNextKd(sendThread);
                processNextKd.Send();
            }
        }
    }
}
