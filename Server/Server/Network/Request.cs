﻿using System.Collections.Generic;
using Server.Database;

namespace Server.Network
{

    public class UserLoginReq
    {
        public User User { get; set; }

        public int RequestName { get; set; }

    }

    public class UpdateQuestionReq
    {
        public Question Question { get; set; }

        public List<Answer> Answers { get; set; }

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

    public class GetListQuestionReq
    {
        public int ContestType { get; set; }

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

    public class LoginExamReq
    {
        public int ExamId { get; set; }

        public string PassWord { get; set; }

        public string UserName { get; set; }

        public int RequestName { get; set; }
    }

    public class StateChangeExamReq
    {
        public int ExamId { get; set; }

        public int RequestName { get; set; }

        public int Role { get; set; }
    }

    public class AnswerKdReq
    {
        public int ExamId { get; set; }

        public string UserName { get; set; }

        public string UserAnswer { get; set; }

        public int RequestName { get; set; }
    }

    public class CalculatePointKdReq
    {
        public int ExamId { get; set; }

        public string UserName { get; set; }

        public int Point { get; set; }

        public int RequestName { get; set; }
    }

    public class NextKdReq
    {
        public int ExamId { get; set; }

        public int RequestName { get; set; }

        public bool IsEndSession { get; set; }
    }

    public class UpdatePackageReq
    {
        public Package Package { get; set; }

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

    public class GetListPackageReq
    {

        public int RequestType { get; set; }

        public int RequestName { get; set; }

    }

}
