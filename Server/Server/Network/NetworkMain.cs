﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using Server.Custom;

namespace Server.Network
{
    public class NetworkMain
    {
        public static TcpListener Server;
        public static IPEndPoint Ipe;
        public static List<Socket> Clients = new List<Socket>();
        public static IPAddress MyIp;
        public static Thread ListenClient;
        public static bool TimeReady = true;
        public static bool FuncEnded = true;

        public static void Init()
        {
            MyIp = GetIp();
            ListenClient = new Thread(ServerListen) { IsBackground = true };
            ListenClient.Start();
        }

        public static void ServerListen()
        {

            Server = new TcpListener(MyIp, DefaultConfig.Port);
            Server.Start();

            while (true)
            {
                Socket sk = Server.AcceptSocket();
                Clients.Add(sk);

                Thread clientProcess = new Thread(ProcessClient) { IsBackground = true };
                clientProcess.Start(sk);

                Log.Success("Chap nhan ket noi tu " + sk.RemoteEndPoint);
            }
        }

        public static void SendBroadCastTime(int examId, int timeQuantity)
        {
            BroadcastTime data = new BroadcastTime
            {
                ExamId = examId,
                TimeQuantity = timeQuantity
            };
            if (!TimeReady)
            {
                TimeReady = true;
                while (!FuncEnded)
                {
                }
                Thread.Sleep(5);
            }
            Thread clientProcess = new Thread(ProcessCountTime) { IsBackground = true };
            clientProcess.Start(data);
        }

        public static void ProcessCountTime(object data)
        {
            TimeReady = false;
            FuncEnded = false;
            BroadcastTime dataFormat = data as BroadcastTime;
            if (dataFormat != null && NetworkMemory.DicExam.ContainsKey(dataFormat.ExamId))
            {
                List<Socket> listUserAddress =
                    NetworkMemory.DicExam[dataFormat.ExamId].ListContestInfo.Select(p => p.Socket).ToList();
                for (int i = dataFormat.TimeQuantity; i >= 0; i--)
                {
                    //khi co vong dem thoi gian moi
                    if (TimeReady)
                    {
                        FuncEnded = true;
                        return;
                    }      

                    BroadcastTimeExam broadcastTimeExam = new BroadcastTimeExam
                    {
                        BroadcastName = (int)BroadcastType.SendTime,
                        TimeCurrentValue = i
                    };
                    var dataJson = JsonConvert.SerializeObject(broadcastTimeExam);
                    foreach (var socket in listUserAddress)
                    {
                        if (!socket.Connected) continue;
                        var stream = new NetworkStream(socket);
                        var writer = new StreamWriter(stream) { AutoFlush = true };
                        writer.WriteLine(dataJson);
                    }
                    Thread.Sleep(990);
                }
            }
            TimeReady = true;
            FuncEnded = true;
        }

        public static void ProcessClient(object obj)
        {
            Socket clientSocket = (Socket)obj;

            var stream = new NetworkStream(clientSocket);
            var reader = new StreamReader(stream);

            while (true)
            {
                try
                {

                    string data = reader.ReadLine();

                    Log.ResReq("Receive request from " + clientSocket.RemoteEndPoint);
                    Thread clientProcess = new Thread(ProcessNav.Navigate) { IsBackground = true };
                    SendThread sendThread = new SendThread
                    {
                        Data = data,
                        Socket = clientSocket
                    };
                    clientProcess.Start(sendThread);
                }
                catch (Exception)
                {
                    Clients.Remove(clientSocket);
                    clientSocket.Close();
                    return;
                }
            }
        }

        public static IPAddress GetIp()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            string currentIp = string.Empty;
            foreach (IPAddress diachi in host.AddressList)
            {
                if (diachi.AddressFamily.ToString() == "InterNetwork")
                {
                    currentIp = diachi.ToString();
                }
            }

            return IPAddress.Parse(currentIp);
        }

        public static void LoginExamBroadcast(List<ContestInfo> listContestInfo)
        {
            List<Socket> listSocket = new List<Socket>();
            List<UserState> listUserState = new List<UserState>();

            foreach (var contestInfo in listContestInfo)
            {
                listSocket.Add(contestInfo.Socket);
                listUserState.Add(contestInfo.UserState);
            }

            BroadcastLoginExam broadcastLoginExam = new BroadcastLoginExam
            {
                BroadcastName = (int)BroadcastType.LoginExam,
                ListUserState = listUserState
            };

            foreach (var socket in listSocket)
            {
                try
                {
                    NetworkUtil.Send(socket, JsonConvert.SerializeObject(broadcastLoginExam));
                }
                catch (Exception e)
                {
                    Log.Error("Error Socket Broadcast in NetworkMain");
                    Log.Error(e.ToString());
                }

            }
            Log.ResReq("Broadcast Login Exam");
        }

        public static void ExamPlayerInit(List<ContestInfo> listContestInfo)
        {
            List<Socket> listSocket = listContestInfo.Select(contestInfo => contestInfo.Socket).ToList();

            BroadcastExamPlayBegin broadcastBeginExam = new BroadcastExamPlayBegin
            {
                BroadcastName = (int)BroadcastType.BeginExam,
                StateExam = (int)State.ExamBegin
            };

            foreach (var socket in listSocket)
            {
                try
                {
                    NetworkUtil.Send(socket, JsonConvert.SerializeObject(broadcastBeginExam));
                }
                catch (Exception e)
                {
                    Log.Error("Error Socket Broadcast in NetworkMain");
                    Log.Error(e.ToString());
                }

            }
            Log.ResReq("Broadcast Login Exam");
        }

        public static void ExamSendQuestion(List<ContestInfo> listContestInfo, BroadcastExamQuestion broadcastExamQuestion)
        {
            List<Socket> listSocket = listContestInfo.Select(contestInfo => contestInfo.Socket).ToList();

            foreach (var socket in listSocket)
            {
                try
                {
                    NetworkUtil.Send(socket, JsonConvert.SerializeObject(broadcastExamQuestion));
                }
                catch (Exception e)
                {
                    Log.Error("Error Socket Broadcast in NetworkMain");
                    Log.Error(e.ToString());
                }

            }
            Log.ResReq("Broadcast Login Exam");
        }

        public static void ExamSendUserInfo(List<ContestInfo> listContestInfo, BroadcastUserInfoExam broadcastUserInfoExam)
        {
            List<Socket> listSocket = listContestInfo.Select(contestInfo => contestInfo.Socket).ToList();

            foreach (var socket in listSocket)
            {
                try
                {
                    NetworkUtil.Send(socket, JsonConvert.SerializeObject(broadcastUserInfoExam));
                }
                catch (Exception e)
                {
                    Log.Error("Error Socket Broadcast in NetworkMain");
                    Log.Error(e.ToString());
                }

            }
            Log.ResReq("Broadcast Login Exam");
        }

    }
}
