﻿using System.Collections.Generic;
using Server.Custom;
using Server.Database;

namespace Server.Network
{
    public class LoginExam
    {
        public List<UserState> ListUserState { get; set; }
        public Exam Exam { get; set; }
    }
}
