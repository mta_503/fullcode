﻿using System.Collections.Generic;
using Server.Custom;

namespace Server.Network
{
    public class NetworkMemory
    {
        public static Dictionary<int, ExamInfo> DicExam = new Dictionary<int, ExamInfo>();

        public static Dictionary<string, int> DicPointUser = new Dictionary<string, int>();

        public static Dictionary<string, int> DicExamInfo = new Dictionary<string, int>();
    }
}
