﻿using System.Collections.Generic;
using Server.Database;

namespace Server.Custom
{
    public class ExamInfo
    {
        public List<ContestInfo> ListContestInfo { get; set; }

        public string UserName { get; set; }

        public List<Question> ListQuestion { get; set; } 

        public int CurrentQuestion { get; set; }

     
    }
}
