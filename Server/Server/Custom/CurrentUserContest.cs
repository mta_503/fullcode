﻿namespace Server.Custom
{
    public class CurrentUserContest
    {
        public int ContestType { get; set; }

        public string UserName { get; set; }
    }
}
