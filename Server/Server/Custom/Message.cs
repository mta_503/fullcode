﻿namespace Server.Custom
{
    public class Message
    {
        public object Data { get; set; }

        public int RequestName { get; set; }

        public int RequestType { get; set; }

    }

}
