﻿using Server.Database;

namespace Server.Custom
{
    public class UserInfoExam
    {
        public User User { get; set; }

        public int Point { get; set; }

        public int Index { get; set; }
    }
}
