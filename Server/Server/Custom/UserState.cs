﻿using Server.Database;

namespace Server.Custom
{
    public class UserState
    {
        public User User { get; set; }
        public int State { get; set; }
    }
}
