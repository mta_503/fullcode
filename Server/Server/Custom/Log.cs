﻿using System;

namespace Server.Custom
{
    public class Log
    {
        public static void Success(string content)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(content);
        }

        public static void Error(string content)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(content);
        }

        public static void Normal(string content)
        {
            Console.ResetColor();
            Console.WriteLine(content);
        }

        public static void ResReq(string content)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(content);
        }

    }
}
