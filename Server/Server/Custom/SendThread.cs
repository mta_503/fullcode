﻿using System;
using System.Net.Sockets;

namespace Server.Custom
{
    public class SendThread : ICloneable
    {
        public string Data { get; set; }

        public Socket Socket { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
