﻿namespace Server.Custom
{
    public enum RequestName
    {
        Login = 1,
        UpdateQuestion = 2,
        GetListQuestion = 3,
        LoginExam = 4,
        StateChangeExam = 5,
        AnswerKd = 6,
        CalculatePointKd = 7,
        NextKd = 8,
        GetListPackage = 9,
        UpdatePackage = 10
    }

    public enum RequestType
    {
        GetAll = 1,
        Insert = 2,
        Update = 3,
        GetByContestType = 4,
        Delete = 5
    }

    public enum ResourceKey
    {
        Success = 1,
        Error = 2,
        FailLogin = 3,
        NotUserInLoginExam = 4,
        ExamNotOpen = 5,
        ExamOnlyOneOwn = 6
    }

    public enum QuestionType
    {
        Choice = 1,
        NotChoice = 2
    }

    public enum ContestType
    {
        Kd = 1,
        Vcnv = 2,
        Tt = 3,
        Vd = 4
    }

    public enum State
    {
        NotReady = 1,
        Ready = 2,
        Start = 3,
        ExamBegin = 4,
        NotPlay = 5,
        Playing = 6,
        DonePlay = 7
    }

    public enum BroadcastType
    {
        LoginExam = 1,
        BeginExam = 2,
        SendQuestion = 3,
        SendUserInfo = 4,
        SendTime = 5,
        AnswerKd = 6,
        CalculPointKd = 7
    }

    public enum Role
    {
        Admin = 1,
        User = 2
    }

    public enum GradeEnum
    {
        Ten = 10,
        Eleven = 11,
        Twelve = 12
    }

    public enum LevelEnum
    {
        Easy = 1,
        Normal = 2,
        Hard = 3
    }
    

    public enum QuestionStatus
    {
        Active = 1,
        InActive = 2
    }

    public enum PackageStatus
    {
        Active = 1,
        InActive = 2
    }

    public enum TimeContestEnum
    {
        Kd = 60
    }

  
}
