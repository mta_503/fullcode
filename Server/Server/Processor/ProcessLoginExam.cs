﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessLoginExam
    {
        private readonly SendThread _sendThread;

        public ProcessLoginExam(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;

            LoginExamReq loginExamReq = NetworkUtil.ParseJson<LoginExamReq>(msgReceive);

            int resourceKey;
            if (loginExamReq.ExamId == 1)
            {
                User sender = DbOperator.GetUserByUserName(loginExamReq.UserName);
                int stateUser = (int) State.NotReady;
                if(sender!=null && sender.Role==(int)Role.Admin) stateUser = (int)State.Start;
                ContestInfo contestInfo = new ContestInfo
                {
                    UserState = new UserState
                    {
                        User = DbOperator.GetUserByUserName(loginExamReq.UserName),
                        State = stateUser
                    },
                    Socket = socket
                };
                if (sender == null)
                    resourceKey = (int) ResourceKey.NotUserInLoginExam;
                else
                {
                    if (!NetworkMemory.DicExam.ContainsKey(loginExamReq.ExamId))
                    {
                        if (sender.Role == (int) Role.Admin)
                        {
                            ExamInfo examInfo = new ExamInfo();
                            examInfo.CurrentQuestion = 0;
                            examInfo.ListContestInfo = new List<ContestInfo>();
                            examInfo.ListQuestion = new List<Question>();
                            examInfo.UserName = String.Empty;
                            NetworkMemory.DicExam[loginExamReq.ExamId] = examInfo;
                            NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo.Add(contestInfo);
                            resourceKey = (int)ResourceKey.Success;
                        }
                        else
                        {
                            resourceKey = (int) ResourceKey.ExamNotOpen;
                        }
                    }
                    else
                    {
                        if (sender.Role == (int) Role.Admin)
                        {
                            resourceKey = (int) ResourceKey.ExamOnlyOneOwn;
                        }
                        else
                        {
                            NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo.Add(contestInfo);
                            resourceKey = (int)ResourceKey.Success;
                        }
                    }
                }
            }
            else
            {
                resourceKey = (int)ResourceKey.FailLogin;
            }

            LoginExamRes loginExamRes = new LoginExamRes
            {
                RequestName = loginExamReq.RequestName,
                ResourceKey = resourceKey
            };
            Log.ResReq("Response Login Exam");
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(loginExamRes));
            if (resourceKey == (int) ResourceKey.Success)
            {
                for (int i = 0; i < NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo.Count; i++)
                {
                    ContestInfo temp = NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo[i];
                    if (!temp.Socket.Connected)
                    {
                        NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo.Remove(temp);
                        i--;
                    }
                }
                NetworkMain.LoginExamBroadcast(NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo);
            }
        }
    }
}
