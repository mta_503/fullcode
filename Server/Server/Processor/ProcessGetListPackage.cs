﻿using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessGetListPackage
    {
        private readonly SendThread _sendThread;

        public ProcessGetListPackage(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;

            GetListPackageReq getListPackageReq = NetworkUtil.ParseJson<GetListPackageReq>(msgReceive);
            GetListPackageRes getListPackageRes = new GetListPackageRes();
            List<Package> listPackage = new List<Package>();

                listPackage = DbOperator.GetListPackage();

            getListPackageRes.ListPackage = listPackage;
            getListPackageRes.RequestName = getListPackageReq.RequestName;
            int bytesSend = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(getListPackageRes)).Length;
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(getListPackageRes));
        }
    }
}
