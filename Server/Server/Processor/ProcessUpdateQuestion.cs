﻿using System.Net.Sockets;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessUpdateQuestion
    {
        private readonly SendThread _sendThread;

        public ProcessUpdateQuestion(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;

            UpdateQuestionReq updateQuestionReq = NetworkUtil.ParseJson<UpdateQuestionReq>(msgReceive);

            var question = updateQuestionReq.Question;

            int resourceKey = 0;
            if (updateQuestionReq.RequestType == (int)RequestType.Insert)
            {
                var questId = DbOperator.InsertQuestion(updateQuestionReq.Question);
                if (questId > 0)
                {
                    resourceKey = (int)ResourceKey.Success;
                }
                else
                {
                    resourceKey = (int)ResourceKey.Error;
                }
                if (question.QuesType == (int)QuestionType.Choice)
                {
                    foreach (var answer in updateQuestionReq.Answers)
                    {
                        answer.QuesId = questId;
                    }
                    var resultAnswer = DbOperator.InsertAnswer(updateQuestionReq.Answers);
                    if (resultAnswer)
                    {
                        resourceKey = (int)ResourceKey.Success;
                    }
                    else
                    {
                        resourceKey = (int)ResourceKey.Error;
                    }
                }
            }
            else if (updateQuestionReq.RequestType == (int)RequestType.Update)
            {
                var resultQuestion = DbOperator.UpdateQuestion(updateQuestionReq.Question);
                if (resultQuestion)
                {
                    resourceKey = (int)ResourceKey.Success;
                }
                else
                {
                    resourceKey = (int)ResourceKey.Error;
                }
                if (question.QuesType == (int)QuestionType.Choice)
                {
                    var resultAnswer = DbOperator.InsertAnswer(updateQuestionReq.Answers);
                    if (resultAnswer)
                    {
                        resourceKey = (int)ResourceKey.Success;
                    }
                    else
                    {
                        resourceKey = (int)ResourceKey.Error;
                    }
                }
            }
            else if (updateQuestionReq.RequestType == (int)RequestType.Delete)
            {
                var resultQuestion = DbOperator.DeleteQuestion(updateQuestionReq.Question.QuesId);
                if (resultQuestion)
                {
                    resourceKey = (int)ResourceKey.Success;
                }
                else
                {
                    resourceKey = (int)ResourceKey.Error;
                }
               
            }


            if (resourceKey == (int)ResourceKey.Success)
            {
                Log.Success("Update question success");
            }
            else
            {
                Log.Error("Update question Fail");
            }

            UpdateQuestionRes updateQuestionRes = new UpdateQuestionRes
            {
                RequestName = updateQuestionReq.RequestName,
                ResourceKey = resourceKey
            };
            Log.ResReq("Response Update Question");
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(updateQuestionRes));
        }
    }
}
