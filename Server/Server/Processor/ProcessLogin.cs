﻿using System.Net.Sockets;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessLogin
    {
        private readonly SendThread _sendThread;

        public ProcessLogin(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;

            UserLoginReq loginReq = NetworkUtil.ParseJson<UserLoginReq>(msgReceive);

            var user = loginReq.User;
            int resourceKey;
            User currentUser = DbOperator.Login(user);
            if (currentUser!=null)
            {
                Log.Success("Login success");
                resourceKey = (int) ResourceKey.Success;
            }
            else
            {
                Log.Error("Login Fail");
                resourceKey = (int) ResourceKey.FailLogin;
            }

            UserLoginRes userLoginRes = new UserLoginRes
            {
                RequestName = loginReq.RequestName,
                ResourceKey = resourceKey,
                User = currentUser
            };
            Log.ResReq("Response");
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(userLoginRes));
        }
    }
}
