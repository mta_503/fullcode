﻿using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessGetListQuestion
    {
        private readonly SendThread _sendThread;

        public ProcessGetListQuestion(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;

            GetListQuestionReq getListQuestionReq = NetworkUtil.ParseJson<GetListQuestionReq>(msgReceive);
            GetListQuestionRes getListQuestionRes = new GetListQuestionRes();
            List<Question> listQuestion = new List<Question>();
            if (getListQuestionReq.RequestType == (int) RequestType.GetByContestType)
            {
                listQuestion = DbOperator.GetListQuestionByContestType(getListQuestionReq.ContestType);
            }
            getListQuestionRes.ListQuestion = listQuestion;
            getListQuestionRes.RequestName = getListQuestionReq.RequestName;
            int bytesSend = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(getListQuestionRes)).Length;
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(getListQuestionRes));
        }
    }
}
