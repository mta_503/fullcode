﻿using System.Net.Sockets;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessUpdatePackage
    {
        private readonly SendThread _sendThread;

        public ProcessUpdatePackage(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;

            UpdatePackageReq updatePackageReq = NetworkUtil.ParseJson<UpdatePackageReq>(msgReceive);

            var package = updatePackageReq.Package;

            int resourceKey = 0;
            if (updatePackageReq.RequestType == (int)RequestType.Insert)
            {
                var questId = DbOperator.InsertPackage(updatePackageReq.Package);
                if (questId > 0)
                {
                    resourceKey = (int)ResourceKey.Success;
                }
                else
                {
                    resourceKey = (int)ResourceKey.Error;
                }
               
            }
            else if (updatePackageReq.RequestType == (int)RequestType.Update)
            {
                var resultPackage = DbOperator.UpdatePackage(updatePackageReq.Package);
                if (resultPackage)
                {
                    resourceKey = (int)ResourceKey.Success;
                }
                else
                {
                    resourceKey = (int)ResourceKey.Error;
                }
        
            }


            if (resourceKey == (int)ResourceKey.Success)
            {
                Log.Success("Update question success");
            }
            else
            {
                Log.Error("Update question Fail");
            }

            UpdatePackageRes updatePackageRes = new UpdatePackageRes
            {
                RequestName = updatePackageReq.RequestName,
                ResourceKey = resourceKey
            };
            Log.ResReq("Response Update Question");
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(updatePackageRes));
        }
    }
}
