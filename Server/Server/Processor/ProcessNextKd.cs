﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessNextKd
    {
        private readonly SendThread _sendThread;

        public ProcessNextKd(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            string msgReceive = _sendThread.Data;

            NextKdReq nextKdReq = NetworkUtil.ParseJson<NextKdReq>(msgReceive);

            if (!NetworkMemory.DicExam.ContainsKey(nextKdReq.ExamId)) return;
             
            ExamInfo examInfo = NetworkMemory.DicExam[nextKdReq.ExamId];
            if (nextKdReq.IsEndSession)
            { examInfo.CurrentQuestion = examInfo.ListQuestion.Count - 1; }
            //send info question and user playing exam
            if (examInfo.CurrentQuestion == examInfo.ListQuestion.Count - 1 )
            {
             

                var user =
               examInfo.ListContestInfo.FirstOrDefault(p => p.UserState.State == (int)State.NotPlay && p.UserState.User.Role != (long)Role.Admin);
                if (user != null)
                {
                    user.UserState.State = (int) State.DonePlay;
                    examInfo.UserName = user.UserState.User.UserName;
                    examInfo.ListQuestion = DbOperator.GetListQuestion();
                    examInfo.CurrentQuestion = -1;
                    if (examInfo.ListQuestion.Count > 0)
                        examInfo.CurrentQuestion++;
                    Question question = examInfo.ListQuestion[examInfo.CurrentQuestion];
                    List<Answer> listAnswer = new List<Answer>();
                    if (question.QuesType == (int)QuestionType.Choice)
                    {
                        listAnswer = DbOperator.GetListAnswerByQuestionId(question.QuesId);
                    }
                    BroadcastExamQuestion broadcastExamQuestion = new BroadcastExamQuestion
                    {
                        Question = question,
                        UserName = user.UserState.User.UserName,
                        QuestionIndex = examInfo.CurrentQuestion,
                        BroadcastName = (int)BroadcastType.SendQuestion,
                        ListAnswer = listAnswer,
                  
                    };
                    NetworkMain.ExamSendQuestion(examInfo.ListContestInfo, broadcastExamQuestion);
                    if (examInfo.CurrentQuestion == 0)
                    {
                        
                        NetworkMain.SendBroadCastTime(nextKdReq.ExamId, (int)TimeContestEnum.Kd);
                    }
                    
                }
            }
            else
            {
                examInfo.CurrentQuestion++;
                Question question = examInfo.ListQuestion[examInfo.CurrentQuestion];
                List<Answer> listAnswer = new List<Answer>();
                if (question.QuesType == (int)QuestionType.Choice)
                {
                    listAnswer = DbOperator.GetListAnswerByQuestionId(question.QuesId);
                }
                BroadcastExamQuestion broadcastExamQuestion = new BroadcastExamQuestion
                {
                    Question = question,
                    UserName = examInfo.UserName,
                    QuestionIndex = examInfo.CurrentQuestion,
                    BroadcastName = (int)BroadcastType.SendQuestion,
                    ListAnswer = listAnswer
                };
                NetworkMain.ExamSendQuestion(examInfo.ListContestInfo, broadcastExamQuestion);

                if (examInfo.CurrentQuestion == 0)
                {
                    NetworkMain.SendBroadCastTime(nextKdReq.ExamId, (int)TimeContestEnum.Kd);
                }
            }

        }
    }
}
