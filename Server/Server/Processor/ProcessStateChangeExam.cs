﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using Newtonsoft.Json;
using Server.Custom;
using Server.Database;
using Server.Network;

namespace Server.Processor
{
    public class ProcessStateChangeExam
    {
        private readonly SendThread _sendThread;

        public ProcessStateChangeExam(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            Socket socket = _sendThread.Socket;
            string msgReceive = _sendThread.Data;
            int resourceKey = (int)ResourceKey.Error;
            StateChangeExamReq stateChangeExamReq = NetworkUtil.ParseJson<StateChangeExamReq>(msgReceive);
            int senderRole = 0;
            if (NetworkMemory.DicExam.ContainsKey(stateChangeExamReq.ExamId))
            {
                for (int i = 0; i < NetworkMemory.DicExam[stateChangeExamReq.ExamId].ListContestInfo.Count; i++)
                {
                    ContestInfo temp = NetworkMemory.DicExam[stateChangeExamReq.ExamId].ListContestInfo[i];
                    if (temp.Socket.Connected &&
                        temp.Socket.RemoteEndPoint.ToString().Equals(socket.RemoteEndPoint.ToString()))
                    {
                        temp.UserState.State = (int)State.Ready;
                        if (temp.UserState.User.Role != null) senderRole = (int)temp.UserState.User.Role;
                        resourceKey = (int)ResourceKey.Success;
                        break;
                    }
                }
            }

            StateChangeExamRes stateChangeExamRes = new StateChangeExamRes
            {
                RequestName = stateChangeExamReq.RequestName,
                ResourceKey = resourceKey
            };

            Log.ResReq("Response State Change Exam");
            NetworkUtil.Send(socket, JsonConvert.SerializeObject(stateChangeExamRes));
            if (resourceKey == (int)ResourceKey.Success)
            {
                ExamInfo examInfo = NetworkMemory.DicExam[stateChangeExamReq.ExamId];
                if (senderRole == (int)Role.User)
                    NetworkMain.LoginExamBroadcast(examInfo.ListContestInfo);
                else
                {
                    List<UserInfoExam> listUserInfoExam = new List<UserInfoExam>();
                    for (int i = 0; i < examInfo.ListContestInfo.Count; i++)
                    {
                        ContestInfo temp = examInfo.ListContestInfo[i];
                        NetworkMemory.DicPointUser[temp.UserState.User.UserName] = 0;
                        examInfo.ListContestInfo[i].UserState.State = (int)State.NotPlay;
                        listUserInfoExam.Add(new UserInfoExam
                        {
                            User = temp.UserState.User,
                            Point = NetworkMemory.DicPointUser[temp.UserState.User.UserName],
                            Index = i + 1
                        });
                    }
                    listUserInfoExam = listUserInfoExam.OrderBy(p => p.Point).ToList();
                    for (int i = 0; i < listUserInfoExam.Count; i++)
                    {
                        listUserInfoExam[i].Index = i + 1;
                    }

                    //send init Khoi dong
                    NetworkMain.ExamPlayerInit(examInfo.ListContestInfo);

                    //send info all user in exam
                    BroadcastUserInfoExam broadcastUserInfoExam = new BroadcastUserInfoExam
                    {
                        ListUserInfoExam = listUserInfoExam,
                        BroadcastName = (int)BroadcastType.SendUserInfo
                    };
                    NetworkMain.ExamSendUserInfo(examInfo.ListContestInfo, broadcastUserInfoExam);

                    //send info question and user playing exam
                    var user =
                        examInfo.ListContestInfo.FirstOrDefault(p => p.UserState.State == (int)State.NotPlay && p.UserState.User.Role!=(long)Role.Admin);
                    if (user != null)
                    {
                        user.UserState.State = (int) State.DonePlay;
                        examInfo.UserName = user.UserState.User.UserName;
                        examInfo.ListQuestion = DbOperator.GetListQuestion();
                        examInfo.CurrentQuestion = -1;
                        if (examInfo.ListQuestion.Count > 0)
                            examInfo.CurrentQuestion++;
                        Question question = examInfo.ListQuestion[examInfo.CurrentQuestion];
                        List<Answer> listAnswer = new List<Answer>();
                        if (question.QuesType == (int)QuestionType.Choice)
                        {
                            listAnswer = DbOperator.GetListAnswerByQuestionId(question.QuesId);
                        }
                        BroadcastExamQuestion broadcastExamQuestion = new BroadcastExamQuestion
                        {
                            Question = question,
                            UserName = user.UserState.User.UserName,
                            QuestionIndex = examInfo.CurrentQuestion,
                            BroadcastName = (int)BroadcastType.SendQuestion,
                            ListAnswer = listAnswer
                        };
                        NetworkMain.ExamSendQuestion(examInfo.ListContestInfo, broadcastExamQuestion);
                        if (examInfo.CurrentQuestion == 0)
                        { NetworkMain.SendBroadCastTime(stateChangeExamReq.ExamId, (int)TimeContestEnum.Kd); }
                    }

                }
            }
        }
    }
}
