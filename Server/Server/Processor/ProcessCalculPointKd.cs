﻿using Newtonsoft.Json;
using Server.Custom;
using Server.Network;

namespace Server.Processor
{
    public class ProcessCalculPointKd
    {
        private readonly SendThread _sendThread;

        public ProcessCalculPointKd(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            string msgReceive = _sendThread.Data;

            CalculatePointKdReq calculatePointKdReq = NetworkUtil.ParseJson<CalculatePointKdReq>(msgReceive);

            if (NetworkMemory.DicExam.ContainsKey(calculatePointKdReq.ExamId))
            {
                if (NetworkMemory.DicPointUser.ContainsKey(calculatePointKdReq.UserName))
                {
                    ExamInfo examInfo = NetworkMemory.DicExam[calculatePointKdReq.ExamId];
                    NetworkMemory.DicPointUser[calculatePointKdReq.UserName] += calculatePointKdReq.Point;
                    if (NetworkMemory.DicPointUser[calculatePointKdReq.UserName] < 0)
                    {
                        NetworkMemory.DicPointUser[calculatePointKdReq.UserName] = 0;
                    }

                    BroadcastPointUserKd msgBroadCast = new BroadcastPointUserKd
                    {
                        Point = NetworkMemory.DicPointUser[calculatePointKdReq.UserName],
                        UserName = calculatePointKdReq.UserName,
                        BroadcastName = (int)BroadcastType.CalculPointKd
                    };
                    foreach (var contestInfo in examInfo.ListContestInfo)
                    {
                        if (contestInfo.Socket.Connected)
                            NetworkUtil.Send(contestInfo.Socket, JsonConvert.SerializeObject(msgBroadCast));
                    }
                }
            }

        }
    }
}
