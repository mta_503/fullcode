﻿using Newtonsoft.Json;
using Server.Custom;
using Server.Network;

namespace Server.Processor
{
    public class ProcessAnswerKd
    {
        private readonly SendThread _sendThread;

        public ProcessAnswerKd(SendThread sendThread)
        {
            _sendThread = sendThread;
        }

        public void Send()
        {
            string msgReceive = _sendThread.Data;

            AnswerKdReq loginExamReq = NetworkUtil.ParseJson<AnswerKdReq>(msgReceive);

            if (NetworkMemory.DicExam.ContainsKey(loginExamReq.ExamId))
            {
                BroadcastAnswerKd msgBroadCast = new BroadcastAnswerKd
                {
                    Answer = loginExamReq.UserAnswer,
                    UserName = loginExamReq.UserName,
                    BroadcastName = (int)BroadcastType.AnswerKd
                };
                foreach (var contestInfo in NetworkMemory.DicExam[loginExamReq.ExamId].ListContestInfo)
                {
                    if (contestInfo.Socket.Connected)
                        NetworkUtil.Send(contestInfo.Socket, JsonConvert.SerializeObject(msgBroadCast));
                }
            }

        }
    }
}
